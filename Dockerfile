# author
#MAINTAINER Imran Qutab
FROM maven:3.6.0-jdk-8 as maven
COPY ./pom.xml ./pom.xml
RUN mvn dependency:go-offline -B 
COPY ./src ./src
RUN mvn package -DskipTests
FROM stakater/java8-alpine:latest
RUN apk update
RUN apk add nano busybox-extras iputils bash tzdata ||:
WORKDIR /cai-uaa
COPY src/main/resources/* ./images/
COPY --from=maven target/cai-uaa-0.0.1-SNAPSHOT.jar ./
CMD ["java", "-jar", "cai-uaa-0.0.1-SNAPSHOT.jar"]
