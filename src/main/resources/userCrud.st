<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Tips</title>
</head>
<body style="padding: 0; margin: 0;">

    <table align="center" width="600" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; padding: 45px 45px 60px; color: #fff; background: linear-gradient(180deg, #13073A 0%, #000000 100%); text-align: center;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                             <a href="#"><img src="cid:logo"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 60px; font-weight: bold; font-size: 20px;">
                            $header$
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px;">
                            <a href="#"><img src="cid:line"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 25px; line-height: 24px;">
                            Dear $name$,<br/><br/>

$body1$ <br/><br/>
$body2$
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 35px;">
                            <a href=$link$ style="width: 130px; height: 35px; background: #4A71EA; border-radius: 300px; color: #fff; display: inline-block; text-decoration: none; line-height: 35px;">
                                $button$
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 25px;">
                            <a href=$link$ style="color: #309AEA; text-decoration: none;">$link$</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</body>
</html>
