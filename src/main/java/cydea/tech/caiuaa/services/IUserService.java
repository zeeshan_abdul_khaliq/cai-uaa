package cydea.tech.caiuaa.services;

import cydea.tech.caiuaa.dtos.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Muhammad Mujtaba, Muhammad Nabeel Rashid
 */
public interface IUserService {

    LoginResponseDto getToken(LoginRequestDto loginRequestDto);

    ResponseDto signUp(UserCreateRequestListDto createRequestListDto, String createdBy, String org, boolean fromOrg);

    ResponseDto updateUser(UserUpdateRequestDto userUpdateRequestDto, String createdBy, String org);

    ResponseDto updateProfile(String link, UserUpdateRequestDto userUpdateRequestDto);

    UserDto getUserDetail(String username);

    UserDto getUserSocDetail(String userId);

    ResponseDto deleteUser(String userId, String createdBy);

    UserListingDto getUsers(String userId, String org);

    ResponseDto resendMagicLink(String magicLink);

    ResponseDto reInviteUser(String userId);

    ResponseDto magicLinkStatus(String magicLink);

    ResponseDto changePassword(String userId, @Valid UserPasswordRequestDto userPasswordRequestDto);

    ResponseDto enableDisablePo(String userId, Boolean status);

    ResponseDto forgotPasswordRequest(ForgotPasswordDto forgotPasswordDto);

    ResponseDto updatePassword(ForgotPasswordLinkDto passwordLinkDto);

    String getClientAdminEmail(String org);

    List<UserInfo> getUsersCreatedBy(String emailId);
}
