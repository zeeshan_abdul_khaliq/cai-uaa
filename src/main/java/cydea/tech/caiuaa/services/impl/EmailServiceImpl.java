package cydea.tech.caiuaa.services.impl;

import cydea.tech.caiuaa.dtos.UserRegistrationAndUpdateDto;
import cydea.tech.caiuaa.services.IEmailService;
import cydea.tech.caiuaa.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STRawGroupDir;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

/**
 * @author Waseem Ud Din
 */
@Slf4j
@Service
public class EmailServiceImpl implements IEmailService {

    @Value("${template.path}")
    private String templatePath;

    @Value("${mail.email}")
    private String email;

    @Value("${mail.password}")
    private String password;

    @Value("${mail.smtp.host}")
    private String mailSmtpHost;

    @Value("${mail.smtp.port}")
    private String mailSmtpPort;

    @Value("${mail.smtp.auth}")
    private String mailSmtpAuth;

    @Value("${mail.smtp.ssl.trust}")
    private String mailSmtpSslTrust;

    @Value("${mail.smtp.starttls.enable}")
    private String mailSmtpStarttlsEnable;

    @Value("${header.main.logo}")
    private String headerMainLogo;

    @Value("${header.line.logo}")
    private String headerLineLogo;

    private Properties props;

    @Async
    @Override
    public void sendMailForUser(String instance, UserRegistrationAndUpdateDto userRegistrationAndUpdateDto) {

        log.info("Sending User Registration email to the server");
        STGroup groupTemplate = new STRawGroupDir(templatePath, '$', '$');
        ST template = groupTemplate.getInstanceOf(instance);
        template.add(Constants.NAME, userRegistrationAndUpdateDto.getUserEmail());
        if (userRegistrationAndUpdateDto.getEmailType().equals(Constants.REGISTRATION)) {
            template.add(Constants.HEADER, Constants.USER_REGISTRATION);
            template.add(Constants.BODY1, Constants.USER_REGISTRATION_BODY1);
            template.add(Constants.BODY2, Constants.USER_REGISTRATION_BODY2);
            template.add(Constants.BUTTON, "Register");
        } else {
            template.add(Constants.HEADER, Constants.PASSWORD_RESET);
            template.add(Constants.BODY1, Constants.PASSWORD_RESET_BODY1);
            template.add(Constants.BODY2, Constants.PASSWORD_RESET_BODY2);
            template.add(Constants.BUTTON, "Reset Password");
        }
        template.add(Constants.LINK, userRegistrationAndUpdateDto.getLink());

        if (props == null)
            initProp();

        Session session = Session.getInstance(props,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(email, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(userRegistrationAndUpdateDto.getUserEmail()));

            message.setSubject(userRegistrationAndUpdateDto.getEmailType().equals(Constants.REGISTRATION) ? Constants.REGISTRATION :
                    Constants.PASSWORD_RESET);
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(template.render(), "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            BodyPart messageBodyPart = new MimeBodyPart();
            DataSource logo = new FileDataSource(headerMainLogo);
            messageBodyPart.setDataHandler(new DataHandler(logo));
            messageBodyPart.addHeader(Constants.CONTENT_ID, "<logo>");
            multipart.addBodyPart(messageBodyPart);
            messageBodyPart = new MimeBodyPart();
            DataSource footer = new FileDataSource(headerLineLogo);
            messageBodyPart.setDataHandler(new DataHandler(footer));
            messageBodyPart.addHeader(Constants.CONTENT_ID, "<line>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            Transport.send(message);
            log.info("Email sent successfully to emailAddress: {}", userRegistrationAndUpdateDto.getUserEmail());

        } catch (MessagingException e) {
            e.printStackTrace();
            log.error("Error while sending email: {}", e.getMessage());
        }
    }

    private void initProp() {
        props = new Properties();
        props.put("mail.smtp.host", mailSmtpHost);
        props.put("mail.smtp.port", mailSmtpPort);
        props.put("mail.smtp.auth", mailSmtpAuth);
        props.put("mail.smtp.ssl.trust", mailSmtpSslTrust);
        props.put("mail.smtp.starttls.enable", mailSmtpStarttlsEnable);
    }
}