package cydea.tech.caiuaa.services.impl;

import cydea.tech.caiuaa.dtos.UserDto;
import cydea.tech.caiuaa.enumerations.SectorEnum;
import cydea.tech.caiuaa.enumerations.SubscriptionTypeEnum;
import cydea.tech.caiuaa.services.ILookupService;
import cydea.tech.caiuaa.utils.Constants;
import cydea.tech.caiuaa.utils.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Slf4j
@Service
public class LookupServiceImpl implements ILookupService {

    private UserUtil userUtil;

    public LookupServiceImpl(UserUtil userUtil) {
        this.userUtil = userUtil;
    }

    @Override
    public List<UserDto> getSocUsers(String userId, String org) {

        List<UserDto> userDtos = new ArrayList<>();
        List<String> roles = userUtil.getUserRoles(userId);

        if (!roles.isEmpty()) {
            if (roles.contains(Constants.MASTER_ADMIN) || roles.contains(Constants.MASTER_ANALYST)) {
                userDtos.addAll(userUtil.getUsersInRole(Constants.MASTER_SOC_ADMIN, org));
                userDtos.addAll(userUtil.getUsersInRole(Constants.MASTER_SOC_ANALYST, org));

            } else if (roles.contains(Constants.DISTRIBUTOR_ADMIN) || roles.contains(Constants.DISTRIBUTOR_ANALYST)) {
                userDtos.addAll(userUtil.getUsersInRole(Constants.DISTRIBUTOR_SOC_ADMIN, org));
                userDtos.addAll(userUtil.getUsersInRole(Constants.DISTRIBUTOR_SOC_ANALYST, org));

            } else if (roles.contains(Constants.PARTNER_ADMIN) || roles.contains(Constants.PARTNER_ANALYST)) {
                userDtos.addAll(userUtil.getUsersInRole(Constants.PARTNER_SOC_ADMIN, org));
                userDtos.addAll(userUtil.getUsersInRole(Constants.PARTNER_SOC_ANALYST, org));
            }
        }
        return userDtos;
    }

    @Override
    public List<String> getSubscriptionTypes() {

        List<String> subscriptionTypes = new ArrayList<>();
        for (SubscriptionTypeEnum subscriptionTypeEnum : SubscriptionTypeEnum.values())
            subscriptionTypes.add(subscriptionTypeEnum.name());
        return subscriptionTypes;
    }

    @Override
    public List<String> getCompanyRoles(String userId) {

        UserResource userResource = userUtil.getUser(userId);
        UserRepresentation user = userUtil.getUserDetail(userResource);
        List<String> companyRoles = null;

        if (user != null) {
            String userRoles = userUtil.getUserRoles(userResource.roles()).toString();
            companyRoles = new ArrayList<>();

            if (userRoles.contains(Constants.MASTER))
                companyRoles.add(StringUtils.capitalize(Constants.DISTRIBUTOR));
            if (userRoles.contains(Constants.DISTRIBUTOR))
                companyRoles.add(StringUtils.capitalize(Constants.PARTNER));
            if (userRoles.contains(Constants.PARTNER))
                companyRoles.add(StringUtils.capitalize(Constants.CLIENT));
        }
        return companyRoles;
    }

    @Override
    public List<String> getAdditionalRoles(String userId) {

        UserResource userResource = userUtil.getUser(userId);
        UserRepresentation user = userUtil.getUserDetail(userResource);
        List<String> additionalRoles = null;

        if (user != null) {
            String userRoles = userUtil.getUserRoles(userResource.roles()).toString();
            additionalRoles = new ArrayList<>();

            String baseRole = userUtil.getUserBaseRole(userResource);

            if (!baseRole.equals(Constants.DISTRIBUTOR) && userRoles.contains(Constants.DISTRIBUTOR_ADMIN))
                additionalRoles.add(StringUtils.capitalize(Constants.DISTRIBUTOR));
            if (!baseRole.equals(Constants.PARTNER) && userRoles.contains(Constants.PARTNER_ADMIN))
                additionalRoles.add(StringUtils.capitalize(Constants.PARTNER));
            if (!baseRole.equals(Constants.CLIENT) && userRoles.contains(Constants.CLIENT_ADMIN))
                additionalRoles.add(StringUtils.capitalize(Constants.CLIENT));
        }
        return additionalRoles;
    }

    @Override
    public List<String> getSocAdditionalRole(String userId) {

        UserResource userResource = userUtil.getUser(userId);
        UserRepresentation user = userUtil.getUserDetail(userResource);
        List<String> additionalRoles = null;

        if (user != null) {
            String userRoles = userUtil.getUserRoles(userResource.roles()).toString();
            additionalRoles = new ArrayList<>();

            if (userRoles.contains(Constants.CLIENT_ADMIN))
                additionalRoles.add(StringUtils.capitalize(Constants.CLIENT));
        }
        return additionalRoles;
    }

    @Override
    public List<String> getSectors() {

        List<String> sectors = new ArrayList<>();
        for (SectorEnum sectorEnum : SectorEnum.values())
            sectors.add(sectorEnum.name());
        return sectors;
    }

    @Override
    public List<String> getAccessRoles(String userId, Boolean soc) {
        return userUtil.getAccessRoles(userId, soc);
    }
}
