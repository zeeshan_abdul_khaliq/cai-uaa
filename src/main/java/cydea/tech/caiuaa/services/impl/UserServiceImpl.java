package cydea.tech.caiuaa.services.impl;

import cydea.tech.caiuaa.dtos.*;
import cydea.tech.caiuaa.enumerations.UserStatusEnum;
import cydea.tech.caiuaa.services.IEmailService;
import cydea.tech.caiuaa.services.IUserService;
import cydea.tech.caiuaa.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * @author Muhammad Mujtaba, Muhammad Nabeel Rashid
 */
@Slf4j
@Service
public class UserServiceImpl implements IUserService {

    @Value("${keycloak.url}")
    private String url;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.secret}")
    private String secret;

    @Value("${keycloak.client}")
    private String client;

    @Value("${mail.expiry}")
    private String mailExpiry;

    @Value("${app.link}")
    private String appLink;

    @Value("${mail.forgot.link}")
    private String forgotLink;

    private final UserUtil userUtil;
    private final KeycloakUtil keycloakUtil;
    private final AES aes;
    private final IEmailService iEmailService;

    public UserServiceImpl(UserUtil userUtil, KeycloakUtil keycloakUtil, AES aes, IEmailService iEmailService) {
        this.userUtil = userUtil;
        this.keycloakUtil = keycloakUtil;
        this.aes = aes;
        this.iEmailService = iEmailService;
    }

    @Override
    public LoginResponseDto getToken(LoginRequestDto loginRequestDto) {

        LoginResponseDto loginResponseDto = new LoginResponseDto();
        LoginDto loginDto = new LoginDto();
        try {
            Keycloak keycloak = KeycloakBuilder.builder().serverUrl(url).realm(realm).grantType(OAuth2Constants.PASSWORD)
                    .clientId(client).clientSecret(secret).username(loginRequestDto.getUsername())
                    .password(loginRequestDto.getPassword()).build();

            AccessTokenResponse accessTokenResponse = keycloak.tokenManager().getAccessToken();
            loginDto.setAccessToken(accessTokenResponse.getToken());
            loginDto.setUsername(userUtil.getUserNameFromToken(accessTokenResponse.getToken()));
            loginDto.setOrganizationName(userUtil.getOrganizationFromToken(accessTokenResponse.getToken()));
            loginDto.setRole(userUtil.getUserBaseRole(userUtil.getRolesFromToken(accessTokenResponse.getToken())));
            loginDto.setExpiresIn(accessTokenResponse.getExpiresIn());
            loginDto.setRefreshToken(accessTokenResponse.getRefreshToken());
            loginDto.setRefreshExpiresIn(accessTokenResponse.getRefreshExpiresIn());
            loginResponseDto.setResponseCode(HttpStatus.OK.value());
            loginResponseDto.setResponseMessage("Token Generated");
            loginResponseDto.setLoginDto(loginDto);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while getting token: {}", e.getStackTrace());
            loginResponseDto.setResponseCode(HttpStatus.UNAUTHORIZED.value());
            loginResponseDto.setResponseMessage("This User Has Been Disabled");
            loginResponseDto.setLoginDto(loginDto);
            return loginResponseDto;
        }
        return loginResponseDto;
    }

    @Override
    public ResponseDto signUp(UserCreateRequestListDto createRequestListDto, String createdBy, String org, boolean fromOrg) {

        Response response = null;
        try {
            List<String> realmRoles = new ArrayList<>();
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);

            if (createRequestListDto.getUserCreateRequestDtos() != null && !createRequestListDto.getUserCreateRequestDtos().isEmpty()) {
                for (UserCreateRequestDto createRequestDto : createRequestListDto.getUserCreateRequestDtos()) {

                    if (!fromOrg) {
                        // Transform User Base Role
                        List<String> userRoles = new ArrayList<>();
                        userRoles.add(createRequestDto.getRole());
                        List<String> transformedRoles = userUtil.transformRoles(userRoles);
                        createRequestDto.setRoles(transformedRoles);

                        // Transform User Additional Roles
                        if (createRequestDto.getAdditionalRoles() != null && !createRequestDto.getAdditionalRoles().isEmpty()) {
                            List<String> transformedAdditionalRoles = userUtil.transformRolesFromAdditionalRoles(createRequestDto.getAdditionalRoles(),
                                    createRequestDto.getRoles());
                            createRequestDto.getRoles().addAll(transformedAdditionalRoles);
                        }
                    }

                    ResponseDto responseDto = userUtil.checkUserAccess(realmResource, createdBy, createRequestDto.getRoles());
                    if (responseDto.getStatusCode() == HttpStatus.FORBIDDEN.value())
                        return responseDto;

                    List<RoleRepresentation> roleRepresentations = realmResource.roles().list();
                    roleRepresentations.forEach(roleRepresentation -> realmRoles.add(roleRepresentation.getName()));

                    for (String r : createRequestDto.getRoles()) {
                        if (!realmRoles.contains(r))
                            return new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Invalid Role Provided");
                    }

                    GroupsResource groupsResource = realmResource.groups();
                    GroupRepresentation groupRepresentation;
                    List<GroupRepresentation> groupRepresentations = null;

                    if (checkIfAnalystRole(createRequestDto.getRoles()) && org != null && !org.isEmpty())
                        groupRepresentations = groupsResource.groups(org, 0, 1);
                    else if (createRequestDto.getCompanyName() != null && !createRequestDto.getCompanyName().isEmpty())
                        groupRepresentations = groupsResource.groups(createRequestDto.getCompanyName(), 0, 1);

                    if (groupRepresentations != null && !groupRepresentations.isEmpty())
                        groupRepresentation = groupRepresentations.get(0);
                    else
                        return new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Company Doesn't Exist");

                    // Check For 1 Soc Admin For An Org
                    if (createRequestDto.getRoles().toString().contains(Constants.SOC_ADMIN)) {
                        List<UserRepresentation> orgUsers = userUtil.getUsersOfOrganization(groupRepresentation.getName());
                        for (UserRepresentation user : orgUsers) {
                            if (userUtil.getUserRoles(user.getId()).toString().contains(Constants.SOC_ADMIN))
                                return new ResponseDto(HttpStatus.CONFLICT.value(), "Soc Admin Already Exist For This Organization");
                        }
                    }

                    UserRepresentation user = new UserRepresentation();
                    user.setUsername(createRequestDto.getEmail());
                    user.setEmail(createRequestDto.getEmail());

                    Map<String, List<String>> attributes = new HashMap<>();
                    attributes.put(Constants.CREATED_BY, Arrays.asList(createdBy));
                    attributes.put(Constants.STATUS, Arrays.asList(UserStatusEnum.INVITED.name()));
                    attributes.put(Constants.OWN_COMPANY, Arrays.asList(groupRepresentation.getName()));
                    user.setAttributes(attributes);
                    user.setGroups(Arrays.asList(groupRepresentation.getName()));

                    // Get realm users
                    UsersResource usersResource = realmResource.users();
                    // Create user (requires manage-users role)
                    response = usersResource.create(user);

                    if (response.getStatus() != HttpStatus.CREATED.value())
                        return new ResponseDto(response.getStatus(), "User Already Exists");

                    String userId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");

                    // Assign realm level role to user
                    for (String r : createRequestDto.getRoles())
                        usersResource.get(userId).roles().realmLevel().add(Arrays.asList(realmResource.roles().get(r).toRepresentation()));

                    // Get Client
                    ClientRepresentation clientRepresentation = realmResource.clients().findByClientId(Constants.CLIENT_ID).get(0);

                    // Get client level role (requires view-clients role)
                    String roles = createRequestDto.getRoles().toString();
                    RoleRepresentation userClientRole;

                    if (roles.contains(Constants.ADMIN))
                        userClientRole = realmResource.clients().get(clientRepresentation.getId()).roles().get(Constants.ADMIN).toRepresentation();
                    else
                        userClientRole = realmResource.clients().get(clientRepresentation.getId()).roles().get(Constants.ANALYST).toRepresentation();

                    // Assign client level role to user
                    usersResource.get(userId).roles().clientLevel(clientRepresentation.getId()).add(Arrays.asList(userClientRole));

                    UserRegistrationAndUpdateDto userRegistrationAndUpdateDto = new UserRegistrationAndUpdateDto();
                    userRegistrationAndUpdateDto.setLink(appLink + aes
                            .encryptString(userId + "^" + LocalDateTime.now().plusMinutes(Long.parseLong(mailExpiry))));
                    userRegistrationAndUpdateDto.setUserEmail(user.getUsername());
                    userRegistrationAndUpdateDto.setEmailType(Constants.REGISTRATION);
                    iEmailService.sendMailForUser(Constants.USER_CRUD, userRegistrationAndUpdateDto);
                }
            } else
                return new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Request Has No Data");

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while adding user: {}", e.getMessage());
            return new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error Occurred While Adding User");

        } finally {
            if (response != null)
                response.close();
        }
        return new ResponseDto(HttpStatus.CREATED.value(), "User Created Successfully");
    }

    @Override
    public ResponseDto updateUser(UserUpdateRequestDto userUpdateRequestDto, String createdBy, String org) {

        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            boolean isloggedUser = false;
            boolean wasSocAnalyst = false;

            // Get realm users
            UsersResource usersResource = realmResource.users();
            // Get user
            UserResource userResource = usersResource.get(userUpdateRequestDto.getUserId());
            UserRepresentation user = userUtil.getUserDetail(userResource);
            if (user == null)
                return new ResponseDto(HttpStatus.NO_CONTENT.value(), null);

            if (user.getUsername().equals(createdBy))
                isloggedUser = true;

            if (isloggedUser) {
                user.setFirstName(userUpdateRequestDto.getFirstName());
                user.setLastName(userUpdateRequestDto.getLastName());
                Map<String, List<String>> attributes = user.getAttributes();
                attributes.put(Constants.DISPLAY_NAME, Arrays.asList(userUpdateRequestDto.getDisplayName()));
                attributes.put(Constants.PHONE_NUMBER, Arrays.asList(userUpdateRequestDto.getPhoneNumber()));
                user.setAttributes(attributes);

            } else {
                List<String> realmRoles = new ArrayList<>();
                List<RoleRepresentation> roleRepresentations = realmResource.roles().list();
                roleRepresentations.forEach(roleRepresentation -> realmRoles.add(roleRepresentation.getName()));

                // Transform User Base Role
                List<String> transformedRoles = userUtil.transformRoles(userUpdateRequestDto.getRoles());
                userUpdateRequestDto.getRoles().clear();
                userUpdateRequestDto.setRoles(transformedRoles);

                // Transform User Additional Role
                if (userUpdateRequestDto.getAdditionalRoles() != null && !userUpdateRequestDto.getAdditionalRoles().isEmpty()) {
                    List<String> transformedAdditionalRoles = userUtil.transformRolesFromAdditionalRoles(userUpdateRequestDto.getAdditionalRoles(),
                            userUpdateRequestDto.getRoles());
                    userUpdateRequestDto.getRoles().addAll(transformedAdditionalRoles);
                }

                for (String r : userUpdateRequestDto.getRoles()) {
                    if (!realmRoles.contains(r))
                        return new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Invalid Role Provided");
                }

                ResponseDto responseDto = userUtil.checkUserAccess(realmResource, createdBy, userUpdateRequestDto.getRoles());
                if (responseDto.getStatusCode() == HttpStatus.FORBIDDEN.value())
                    return responseDto;

                // Role setting
                List<RoleRepresentation> roleRepresentationList = usersResource.get(userUpdateRequestDto.getUserId()).roles().realmLevel().listEffective();
                wasSocAnalyst = roleRepresentationList.stream().anyMatch(r -> r.getName().contains(Constants.SOC_ANALYST));

                usersResource.get(userUpdateRequestDto.getUserId()).roles().realmLevel().remove(roleRepresentationList);
                for (String r : userUpdateRequestDto.getRoles())
                    usersResource.get(userUpdateRequestDto.getUserId()).roles().realmLevel().add(Arrays.asList(realmResource.roles().get(r).toRepresentation()));

                if (userUpdateRequestDto.getEnabled() != null && !(user.isEnabled().equals(userUpdateRequestDto.getEnabled()))) {
                    user.setEnabled(userUpdateRequestDto.getEnabled());
                    Map<String, List<String>> attributes = user.getAttributes();

                    if (userUpdateRequestDto.getEnabled().booleanValue())
                        attributes.put(Constants.STATUS, Arrays.asList(UserStatusEnum.ACTIVE.name()));
                    else
                        attributes.put(Constants.STATUS, Arrays.asList(UserStatusEnum.DISABLED.name()));
                    user.setAttributes(attributes);
                }
            }
            // Update user
            userResource.update(user);

            if (userUpdateRequestDto.getEnabled() != null && !userUpdateRequestDto.getEnabled().booleanValue()) {
                if (userUpdateRequestDto.getRoles().toString().contains(Constants.SOC_ADMIN)) {
                    // Set assigned soc attribute false if no-soc left for organization
                    userUtil.updateSocAssignedAttribute(user.getId(), user.getAttributes().get(Constants.OWN_COMPANY).get(0), realmResource);
                    // Assign soc analyst organizations to soc-admin
                } else if (userUpdateRequestDto.getRoles().toString().contains(Constants.SOC_ANALYST))
                    userUtil.assignSocAnalystOrgsToSocAdmin(userUpdateRequestDto.getUserId(), user.getAttributes().get(Constants.OWN_COMPANY).get(0));
            }

            if (wasSocAnalyst && userUpdateRequestDto.getRoles().toString().contains(Constants.SOC_ADMIN))
                userUtil.assignUnAssignedOrgsToSocAdmin(userResource, user.getAttributes().get(Constants.OWN_COMPANY).get(0), realmResource);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while updating user: {}", e.getMessage());
            return new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error Occurred While Updating User");
        }
        return new ResponseDto(HttpStatus.OK.value(), "User Updated Successfully");
    }

    @Override
    public ResponseDto updateProfile(String link, UserUpdateRequestDto userUpdateRequestDto) {

        try {
            userUpdateRequestDto.setUserId(null);
            if (link != null) {
                String response = aes.decryptString(link);
                String[] responseArray = response.split("\\^");
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime mailTime = LocalDateTime.parse(responseArray[1]);
                Long diff = mailTime.until(now, ChronoUnit.MINUTES);
                Long expiry = Long.parseLong(mailExpiry);
                if (diff < expiry)
                    log.debug("Valid link detected");
                else
                    return new ResponseDto(HttpStatus.FORBIDDEN.value(), "Email link has expired");
                userUpdateRequestDto.setUserId(responseArray[0]);

                Keycloak keycloak = keycloakUtil.getInstance();
                RealmResource realmResource = keycloak.realm(realm);
                // Get realm users
                UsersResource usersResource = realmResource.users();
                // Get user
                UserResource userResource = usersResource.get(userUpdateRequestDto.getUserId());
                UserRepresentation user = userUtil.getUserDetail(userResource);
                if (user == null)
                    return new ResponseDto(HttpStatus.NO_CONTENT.value(), null);

                //Set attributes
                user.setFirstName(userUpdateRequestDto.getFirstName());
                user.setLastName(userUpdateRequestDto.getLastName());
                Map<String, List<String>> attributes = user.getAttributes();
                attributes.put(Constants.DISPLAY_NAME, Arrays.asList(userUpdateRequestDto.getDisplayName()));
                attributes.put(Constants.PHONE_NUMBER, Arrays.asList(userUpdateRequestDto.getPhoneNumber()));
                attributes.put(Constants.STATUS, Arrays.asList(UserStatusEnum.ACTIVE.name()));
                user.setAttributes(attributes);
                user.setEnabled(true);

                //Set password
                CredentialRepresentation passwordCred = new CredentialRepresentation();
                passwordCred.setTemporary(false);
                passwordCred.setType(CredentialRepresentation.PASSWORD);
                passwordCred.setValue(userUpdateRequestDto.getPassword());
                userResource.resetPassword(passwordCred);

                //Update user
                userResource.update(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while updating user: {}", e.getMessage());
            return new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error Occurred While Updating User");
        }
        return new ResponseDto(HttpStatus.OK.value(), "User Updated Successfully");
    }

    @Override
    public UserDto getUserDetail(String username) {

        UserDto userDto = null;
        List<String> roles = new ArrayList<>();
        String role = null;

        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            // Get realm users
            UsersResource usersResource = realmResource.users();
            // Get user
            List<UserRepresentation> userRepresentations = usersResource.search(username);
            UserRepresentation user;

            if (userRepresentations.isEmpty())
                return null;
            else {
                user = userRepresentations.get(0);
                userDto = new UserDto();
            }

            //Set attributes
            userDto.setUserId(user.getId());
            userDto.setFirstName(user.getFirstName());
            userDto.setLastName(user.getLastName());
            Map<String, List<String>> attributes = user.getAttributes();

            UserResource userResource = usersResource.get(user.getId());

            userResource.roles().realmLevel().listEffective().forEach(roleTemp -> {
                if (!roleTemp.getName().equals(Constants.DEFAULT_ROLE_CAI_UAA))
                    roles.add(roleTemp.getName());
            });

            String userRoles = roles.toString();

            if (userRoles.contains(Constants.MASTER))
                role = Constants.MASTER;
            else if (userRoles.contains(Constants.DISTRIBUTOR))
                role = Constants.DISTRIBUTOR;
            else if (userRoles.contains(Constants.PARTNER))
                role = Constants.PARTNER;

            if (role != null)
                userDto.setAdditionalRoles(userUtil.getUserAdditionalRoles(roles, role));

            userDto.setRole(userUtil.getUserBaseRole(userUtil.getUserRoles(userResource.roles())));
            userDto.setDisplayName(attributes.containsKey(Constants.DISPLAY_NAME) ? attributes.get(Constants.DISPLAY_NAME).get(0) : null);
            userDto.setPhoneNumber(attributes.containsKey(Constants.PHONE_NUMBER) ? attributes.get(Constants.PHONE_NUMBER).get(0) : null);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while getting user: {}", e.getMessage());
        }
        return userDto;
    }

    @Override
    public UserDto getUserSocDetail(String userId) {

        UserDto userDto = null;
        List<OrganizationViewDto> organizationViewDtos = new ArrayList<>();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            // Get realm users
            UsersResource usersResource = realmResource.users();
            // Get user
            UserResource userResource = usersResource.get(userId);
            UserRepresentation user = userUtil.getUserDetail(userResource);
            if (user == null)
                return null;
            Map<String, List<String>> userAttributes = user.getAttributes();

            // Set attributes
            userDto = new UserDto();
            userDto.setUserId(user.getId());
            userDto.setFirstName(user.getFirstName());
            userDto.setLastName(user.getLastName());
            userDto.setEmailId(user.getEmail());
            userDto.setRole(userUtil.getUserBaseRole(userUtil.getUserRoles(userResource.roles())));

            // Get user groups
            List<GroupRepresentation> groups = userResource.groups();

            if (!groups.isEmpty()) {
                GroupsResource groupsResource = realmResource.groups();

                for (GroupRepresentation group : groups) {
                    if (!group.getName().equals(userAttributes.get(Constants.OWN_COMPANY).get(0))) {

                        OrganizationViewDto organizationDto = new OrganizationViewDto();
                        GroupResource groupResource = groupsResource.group(group.getId());
                        group = groupResource.toRepresentation();
                        Map<String, List<String>> attributes = group.getAttributes();

                        organizationDto.setId(group.getId());
                        organizationDto.setCompanyName(group.getName());
                        organizationDto.setSubscriptionType(attributes.get(Constants.SUBSCRIPTION_TYPE).get(0));
                        organizationDto.setSubscriptionStartDate(attributes.get(Constants.SUBSCRIPTION_START_DATE).get(0));
                        organizationDto.setSubscriptionEndDate(attributes.get(Constants.SUBSCRIPTION_END_DATE).get(0));
                        organizationDto.setSector(attributes.get(Constants.SECTOR).get(0));
                        organizationDto.setCompanyBaseRole(userUtil.getOrganizationBaseRole(attributes.get(Constants.COMPANY_ROLES)));
                        organizationDto.setOrganizationStatus(Boolean.valueOf(attributes.get(Constants.ORGANIZATION_STATUS).get(0)));
                        organizationDto.setAccountManager(userUtil.getUserByUsername(attributes.get(Constants.CREATED_BY).get(0))
                                .getAttributes().get(Constants.DISPLAY_NAME).get(0));
                        organizationViewDtos.add(organizationDto);
                    }
                }
                userDto.setOrganizationViewDtos(organizationViewDtos);
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while getting soc user: {}", e.getMessage());
        }
        return userDto;
    }

    @Override
    public ResponseDto resendMagicLink(String magicLink) {

        ResponseDto responseDto;
        try {
            String response = aes.decryptString(magicLink);
            String[] responseArray = response.split("\\^");
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime mailTime = LocalDateTime.parse(responseArray[1]);
            Long diff = mailTime.until(now, ChronoUnit.MINUTES);
            Long expiry = Long.parseLong(mailExpiry);

            if (diff < expiry)
                responseDto = new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Session is not timed out");
            else {
                String userId = responseArray[0];
                UserRegistrationAndUpdateDto userRegistrationAndUpdateDto = new UserRegistrationAndUpdateDto();
                userRegistrationAndUpdateDto.setLink(appLink + aes
                        .encryptString(userId + "^" + LocalDateTime.now().plusMinutes(Long.parseLong(mailExpiry))));

                userRegistrationAndUpdateDto.setEmailType(Constants.REGISTRATION);

                UserResource userResource = userUtil.getUser(userId);
                UserRepresentation user = userUtil.getUserDetail(userResource);

                if (user == null || user.getEmail() == null || user.getUsername() == null)
                    return new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Invalid request or Resource not exists");

                userRegistrationAndUpdateDto.setUserEmail(user.getEmail());
                iEmailService.sendMailForUser(Constants.USER_CRUD, userRegistrationAndUpdateDto);
                responseDto = new ResponseDto(HttpStatus.OK.value(), "Verification Email Sent!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Link verification error: {}", e.getMessage());
            responseDto = new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error occurred while verifying link");
        }
        return responseDto;
    }

    @Override
    public ResponseDto reInviteUser(String userId) {

        ResponseDto responseDto;
        try {
            UserResource userResource = userUtil.getUser(userId);
            UserRepresentation user = userUtil.getUserDetail(userResource);

            UserRegistrationAndUpdateDto userRegistrationAndUpdateDto = new UserRegistrationAndUpdateDto();
            userRegistrationAndUpdateDto.setLink(appLink + aes
                    .encryptString(userId + "^" + LocalDateTime.now().plusMinutes(Long.parseLong(mailExpiry))));
            userRegistrationAndUpdateDto.setUserEmail(user.getUsername());
            userRegistrationAndUpdateDto.setEmailType(Constants.REGISTRATION);
            iEmailService.sendMailForUser(Constants.USER_CRUD, userRegistrationAndUpdateDto);
            responseDto = new ResponseDto(HttpStatus.OK.value(), "Verification Email Sent!");

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while sending user re-invite: {}", e.getMessage());
            responseDto = new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error occurred while sending user re-invite");
        }
        return responseDto;
    }

    @Override
    public ResponseDto magicLinkStatus(String magicLink) {

        ResponseDto responseDto;
        try {
            String response = aes.decryptString(magicLink);
            String[] responseArray = response.split("\\^");
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime mailTime = LocalDateTime.parse(responseArray[1]);
            Long diff = mailTime.until(now, ChronoUnit.MINUTES);
            Long expiry = Long.parseLong(mailExpiry);

            UserResource userResource = userUtil.getUser(responseArray[0]);
            UserRepresentation user = userUtil.getUserDetail(userResource);
            if (user == null)
                responseDto = new ResponseDto(HttpStatus.CONFLICT.value(), "This Link Is Not Valid Anymore, Please Ask Your Support Team To Send You A New Invite");
            else if (user.isEnabled().booleanValue())
                responseDto = new ResponseDto(HttpStatus.CONFLICT.value(), "You Have Already Registered");
            else if (diff < expiry)
                responseDto = new ResponseDto(HttpStatus.OK.value(), "Provided Link Is Valid");
            else
                responseDto = new ResponseDto(HttpStatus.UNAUTHORIZED.value(), "This Link Has Expired");

        } catch (Exception e) {
            e.printStackTrace();
            log.error("link verification error: {}", e.getMessage());
            responseDto = new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error occurred while verifying link");
        }
        return responseDto;
    }

    @Override
    public ResponseDto deleteUser(String userId, String createdBy) {

        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            List<String> roles = new ArrayList<>();
            RealmResource realmResource = keycloak.realm(realm);

            // Get realm users
            UsersResource usersResource = realmResource.users();
            // Get user
            UserResource userResource = usersResource.get(userId);
            // Delete user
            UserRepresentation user = userUtil.getUserDetail(userResource);
            if (user == null)
                return new ResponseDto(HttpStatus.NO_CONTENT.value(), null);

            List<RoleRepresentation> roleRepresentations = userResource.roles().realmLevel().listEffective();
            roleRepresentations.forEach(roleRepresentation -> roles.add(roleRepresentation.getName()));

            ResponseDto responseDto = userUtil.checkUserAccess(realmResource, createdBy, roles);

            if (responseDto.getStatusCode() == HttpStatus.OK.value()) {

                if (roles.toString().contains(Constants.SOC_ADMIN)) {
                    // Set assigned soc attribute false if no-soc left for organization
                    userUtil.updateSocAssignedAttribute(user.getId(), user.getAttributes().get(Constants.OWN_COMPANY).get(0), realmResource);
                    // Assign soc analyst organizations to soc-admin
                } else if (roles.toString().contains(Constants.SOC_ANALYST))
                    userUtil.assignSocAnalystOrgsToSocAdmin(userId, user.getAttributes().get(Constants.OWN_COMPANY).get(0));

                userResource.remove();
            } else
                return responseDto;

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while deleting user: {}", e.getMessage());
            return new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error Occurred While Deleting User");
        }
        return new ResponseDto(HttpStatus.OK.value(), "User Deleted Successfully");
    }

    @Override
    public UserListingDto getUsers(String userId, String org) {

        UserListingDto userListingDto = null;
        OrganizationUserListDto organizationUserListDto;
        SocUserListDto socUserListDto;
        try {
            UserResource currentUserResource = userUtil.getUser(userId);
            String currentUserRole = userUtil.getUserRoles(currentUserResource.roles()).toString();
            List<UserRepresentation> userRepresentations = userUtil.getUsersOfOrganization(org);

            if (!userRepresentations.isEmpty()) {
                userListingDto = new UserListingDto();
                organizationUserListDto = new OrganizationUserListDto();
                socUserListDto = new SocUserListDto();

                List<UserDto> orgActiveUsers = new ArrayList<>();
                List<UserDto> orgDisabledUsers = new ArrayList<>();
                List<UserDto> orgInvitedUsers = new ArrayList<>();

                List<UserDto> socActiveUsers = new ArrayList<>();
                List<UserDto> socDisabledUsers = new ArrayList<>();
                List<UserDto> socInvitedUsers = new ArrayList<>();

                UserDto userDto;

                for (UserRepresentation user : userRepresentations) {
                    userDto = new UserDto();
                    Map<String, List<String>> attributes = user.getAttributes();

                    userDto.setUserId(user.getId());
                    if (user.getFirstName() != null)
                        userDto.setFirstName(user.getFirstName());
                    if (user.getLastName() != null)
                        userDto.setLastName(user.getLastName());
                    userDto.setEmailId(user.getEmail());

                    UserResource userResource = userUtil.getUser(user.getId());
                    List<String> roles = userUtil.getUserRoles(userResource.roles());
                    userDto.setRole(userUtil.getUserBaseRole(roles));
                    String status = attributes.get(Constants.STATUS).get(0);

                    if (roles.toString().contains(Constants.SOC) && attributes.get(Constants.OWN_COMPANY).get(0).equals(org)) {
                        if (status.equals(UserStatusEnum.ACTIVE.name()))
                            socActiveUsers.add(userDto);
                        else if (status.equals(UserStatusEnum.DISABLED.name()))
                            socDisabledUsers.add(userDto);
                        else if (status.equals(UserStatusEnum.INVITED.name()))
                            socInvitedUsers.add(userDto);

                    } else if (!roles.toString().contains(Constants.SOC) && !currentUserRole.contains(Constants.SOC)
                            && currentUserRole.contains(Constants.ADMIN) || (currentUserRole.contains(Constants.ANALYST)
                            && !roles.toString().contains(Constants.ADMIN))) {

                        if (status.equals(UserStatusEnum.ACTIVE.name()))
                            orgActiveUsers.add(userDto);
                        else if (status.equals(UserStatusEnum.DISABLED.name()))
                            orgDisabledUsers.add(userDto);
                        else if (status.equals(UserStatusEnum.INVITED.name()))
                            orgInvitedUsers.add(userDto);
                    }
                }
                organizationUserListDto.setActiveUsers(orgActiveUsers);
                organizationUserListDto.setDisabledUsers(orgDisabledUsers);
                organizationUserListDto.setInvitedUsers(orgInvitedUsers);

                socUserListDto.setActiveUsers(socActiveUsers);
                socUserListDto.setDisabledUsers(socDisabledUsers);
                socUserListDto.setInvitedUsers(socInvitedUsers);

                userListingDto.setOrganizationUserListDto(organizationUserListDto);
                userListingDto.setSocUserListDto(socUserListDto);

                if (currentUserRole.contains(Constants.SOC))
                    userListingDto.setSoc(true);
                else
                    userListingDto.setSoc(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while getting users list: {}", e.getMessage());
        }
        return userListingDto;
    }

    private boolean checkIfAnalystRole(List<String> roles) {

        boolean exist = false;
        if (roles.contains(Constants.MASTER_ANALYST) || roles.contains(Constants.DISTRIBUTOR_ANALYST)
                || roles.contains(Constants.PARTNER_ANALYST) || roles.contains(Constants.CLIENT_ANALYST)
                || roles.contains(Constants.MASTER_SOC_ANALYST) || roles.contains(Constants.DISTRIBUTOR_SOC_ANALYST)
                || roles.contains(Constants.PARTNER_SOC_ANALYST))
            exist = true;
        return exist;
    }

    @Override
    public ResponseDto changePassword(String userId, @Valid UserPasswordRequestDto userPasswordRequestDto) {

        log.info("User update service");
        ResponseDto responseDto = new ResponseDto();
        Keycloak keycloak;
        try {
            keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            UserResource userResource = usersResource.get(userId);
            UserRepresentation userRepresentation = userResource.toRepresentation();
            LoginResponseDto loginResponseDto = getToken(new LoginRequestDto(userRepresentation.getEmail(), userPasswordRequestDto.getOldPassword()));

            if (loginResponseDto.getResponseCode() == HttpStatus.OK.value()) {
                CredentialRepresentation passwordCred = new CredentialRepresentation();
                passwordCred.setTemporary(false);
                passwordCred.setType(CredentialRepresentation.PASSWORD);
                passwordCred.setValue(userPasswordRequestDto.getPassword());
                userResource.resetPassword(passwordCred);

                responseDto.setStatusCode(HttpStatus.OK.value());
                responseDto.setResponseMsg(ApplicationConstants.PASSWORD_CHANGED_SUCCESSFULLY);
            } else {
                responseDto.setStatusCode(HttpStatus.FORBIDDEN.value());
                responseDto.setResponseMsg(ApplicationConstants.PASSWORD_MISMATCH);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseDto.setResponseMsg(ApplicationConstants.FAILURE);
        }
        return responseDto;
    }

    @Override
    public ResponseDto enableDisablePo(String userId, Boolean status) {

        log.info("Enable user");
        ResponseDto responseDto = new ResponseDto();
        Keycloak keycloak;
        try {
            keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            UserResource userResource = usersResource.get(userId);
            UserRepresentation userRepresentation = userResource.toRepresentation();
            //enable disable user
            userRepresentation.setEnabled(status);

            //Getting and updating attributes
            Map<String, List<String>> attributes = userRepresentation.getAttributes();
            attributes.put(Constants.STATUS, Arrays.asList(status == Boolean.TRUE ? UserStatusEnum.ACTIVE.name() : UserStatusEnum.DISABLED.name()));
            userRepresentation.setAttributes(attributes);

            userResource.update(userRepresentation);

            responseDto.setStatusCode(HttpStatus.OK.value());
            responseDto.setResponseMsg(ApplicationConstants.USER_STATUS_CHANGED);

        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseDto.setResponseMsg(ApplicationConstants.FAILURE);
        }
        return responseDto;
    }

    @Override
    public ResponseDto forgotPasswordRequest(ForgotPasswordDto forgotPasswordDto) {

        log.info("User forgot password service");
        Keycloak keycloak = keycloakUtil.getInstance();
        ResponseDto responseDto = new ResponseDto();
        EncrptedDto encrptedDto = new EncrptedDto();
        try {
            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            List<UserRepresentation> userRepresentation = usersResource.search("", null, null, forgotPasswordDto.getUserEmail(),
                    -1, -1);
            encrptedDto.setLocalDateTime(LocalDateTime.now());
            encrptedDto.setUserId(userRepresentation.get(0).getId());
            // User Notification on forgot account credentials

            UserRegistrationAndUpdateDto userRegistrationAndUpdateDto = new UserRegistrationAndUpdateDto();
            userRegistrationAndUpdateDto.setLink(forgotLink + aes
                    .encryptString(userRepresentation.get(0).getId() + "^" + LocalDateTime.now().plusMinutes(Long.parseLong(mailExpiry))));
            userRegistrationAndUpdateDto.setUserEmail(userRepresentation.get(0).getEmail());
            userRegistrationAndUpdateDto.setEmailType(Constants.FORGOT_PASSWORD);
            iEmailService.sendMailForUser(Constants.USER_CRUD, userRegistrationAndUpdateDto);

            responseDto.setStatusCode(HttpStatus.OK.value());
            responseDto.setResponseMsg(ApplicationConstants.MESSAGE_CHANGE_PASSWORD_REQUEST);
        } catch (IndexOutOfBoundsException e) {
            log.error("forgotPassword: " + e);
            responseDto.setStatusCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
            responseDto.setResponseMsg(ApplicationConstants.FAILURE_USER_DOES_NOT_EXIST);
            return responseDto;
        } catch (Exception e) {
            log.error("forgotPassword: " + e);
            responseDto.setStatusCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
            responseDto.setResponseMsg(e.getMessage());
            return responseDto;
        }
        return responseDto;
    }

    @Override
    public ResponseDto updatePassword(ForgotPasswordLinkDto passwordLinkDto) {
        ResponseDto responseDto;
        try {
            String response = aes.decryptString(passwordLinkDto.getLink());
            String[] responseArray = response.split("\\^");
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime mailTime = LocalDateTime.parse(responseArray[1]);
            Long diff = mailTime.until(now, ChronoUnit.MINUTES);
            Long expiry = Long.parseLong(mailExpiry);

            if (diff > expiry)
                responseDto = new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Session is not timed out");
            else {
                String userId = responseArray[0];
                Keycloak keycloak = keycloakUtil.getInstance();
                RealmResource realmResource = keycloak.realm(realm);

                UsersResource usersResource = realmResource.users();
                UserResource userResource = usersResource.get(userId);
                UserRepresentation user = userResource.toRepresentation();

                if (user == null || user.getEmail() == null || user.getUsername() == null)
                    return new ResponseDto(HttpStatus.BAD_REQUEST.value(), "Invalid request or Resource not exists");


                //Set password
                CredentialRepresentation passwordCred = new CredentialRepresentation();
                passwordCred.setTemporary(false);
                passwordCred.setType(CredentialRepresentation.PASSWORD);
                passwordCred.setValue(passwordLinkDto.getUpdatedPassword());
                userResource.resetPassword(passwordCred);

                //Update user
                userResource.update(user);
                responseDto = new ResponseDto(HttpStatus.OK.value(), ApplicationConstants.PASSWORD_CHANGED_SUCCESSFULLY);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Link verification error: {}", e.getMessage());
            responseDto = new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error occurred while verifying link");
        }
        return responseDto;
    }

    @Override
    public String getClientAdminEmail(String org) {

        String emailId = null;
        List<UserRepresentation> userRepresentations = userUtil.getUsersOfOrganization(org);
        if (!userRepresentations.isEmpty()) {
            for (UserRepresentation user : userRepresentations) {
                String roles = userUtil.getUserRoles(user.getId()).toString();
                if (roles.contains(Constants.CLIENT_ADMIN)) {
                    emailId = user.getEmail();
                    break;
                }
            }
        }
        return emailId;
    }

    @Override
    public List<UserInfo> getUsersCreatedBy(String emailId) {

        List<UserInfo> userInfos = new ArrayList<>();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            List<UserRepresentation> userRepresentations = usersResource.list();

            for (UserRepresentation user : userRepresentations) {
                if (user.getAttributes().get(Constants.CREATED_BY).get(0).equals(emailId)) {
                    UserInfo userInfo = new UserInfo();
                    userInfo.setEmailId(user.getEmail());
                    userInfo.setDisplayName(user.getAttributes().get(Constants.DISPLAY_NAME).get(0));
                    userInfos.add(userInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfos;
    }
}
