package cydea.tech.caiuaa.services.impl;

import cydea.tech.caiuaa.dtos.OrganizationDto;
import cydea.tech.caiuaa.dtos.OrganizationViewDto;
import cydea.tech.caiuaa.dtos.ResponseDto;
import cydea.tech.caiuaa.dtos.UserCreateRequestDto;
import cydea.tech.caiuaa.enumerations.SectorEnum;
import cydea.tech.caiuaa.enumerations.SubscriptionTypeEnum;
import cydea.tech.caiuaa.services.IOrganizationService;
import cydea.tech.caiuaa.utils.Constants;
import cydea.tech.caiuaa.utils.KeycloakUtil;
import cydea.tech.caiuaa.utils.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.*;

/**
 * @author Muhammad Nabeel Rashid,Muhammad Mujtaba
 */
@Slf4j
@Service
public class OrganizationServiceImpl implements IOrganizationService {

    @Value("${keycloak.realm}")
    private String realm;

    private KeycloakUtil keycloakUtil;

    private UserUtil userUtil;

    private UserServiceImpl userService;

    public OrganizationServiceImpl(KeycloakUtil keycloakUtil, UserUtil userUtil, UserServiceImpl userService) {
        this.keycloakUtil = keycloakUtil;
        this.userUtil = userUtil;
        this.userService = userService;
    }

    @Override
    public ResponseDto signUp(OrganizationDto organizationDto, String userId, String createdBy, String org) {

        ResponseDto responseDto = new ResponseDto();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            UserResource userResource = userUtil.getUser(userId);

            UserCreateRequestDto createRequestDto = organizationDto.getCreateRequestListDto().getUserCreateRequestDtos().get(0);
            createRequestDto.setCompanyName(organizationDto.getCompanyName());

            // Check If User Exist For Any Other Organization
            UserRepresentation user = userUtil.getUserByUsername(organizationDto.getCreateRequestListDto().getUserCreateRequestDtos().get(0).getEmail());
            if (user != null) {
                responseDto.setStatusCode(HttpStatus.CONFLICT.value());
                responseDto.setResponseMsg("User Already Exist For Another Organization");
                return responseDto;
            }

            // Merged Company Additional Roles
            if (organizationDto.getAdditionalRoles() != null && !organizationDto.getAdditionalRoles().isEmpty())
                organizationDto.getCompanyRoles().addAll(organizationDto.getAdditionalRoles());

            // Check User Has Access To Add This Company Role
            if (!hasAccess(userResource, organizationDto.getCompanyRoles())) {
                responseDto.setStatusCode(HttpStatus.FORBIDDEN.value());
                responseDto.setResponseMsg("User Don't Have Access To Added This Company Role");
                return responseDto;
            }

            // Transform User Role From Company Role
            if (organizationDto.getCompanyRoles() != null && !organizationDto.getCompanyRoles().isEmpty()) {
                List<String> transformedRoles = userUtil.transformRolesFromCompanyRoles(organizationDto.getCompanyRoles(),
                        createRequestDto.getRole());
                createRequestDto.setRoles(transformedRoles);
            } else {
                responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
                responseDto.setResponseMsg("Company Role Can't Be Null");
                return responseDto;
            }

            // Check User Has Access To Add This User In This Organization
            ResponseDto responseUserAccess = userUtil.checkUserAccess(realmResource, createdBy, createRequestDto.getRoles());
            if (responseUserAccess.getStatusCode() == HttpStatus.FORBIDDEN.value())
                return responseUserAccess;

            GroupsResource groupsResource = realmResource.groups();
            GroupRepresentation groupRepresentation = new GroupRepresentation();
            groupRepresentation.setName(organizationDto.getCompanyName());

            // Adding Company Attributes
            Map<String, List<String>> groupAttributes = new HashMap<>();
            groupAttributes.put(Constants.SUBSCRIPTION_TYPE, Arrays.asList(SubscriptionTypeEnum.valueOf(organizationDto.getSubscriptionType()).name()));
            groupAttributes.put(Constants.COMPANY_ROLES, organizationDto.getCompanyRoles());
            groupAttributes.put(Constants.SECTOR, Arrays.asList(SectorEnum.valueOf(organizationDto.getSector()).name()));
            groupAttributes.put(Constants.SUBSCRIPTION_START_DATE, Arrays.asList(organizationDto.getSubscriptionStartDate()));
            groupAttributes.put(Constants.SUBSCRIPTION_END_DATE, Arrays.asList(organizationDto.getSubscriptionEndDate()));
            groupAttributes.put(Constants.ORGANIZATION_STATUS, Arrays.asList(Boolean.TRUE.toString()));
            groupAttributes.put(Constants.TIP, Arrays.asList(String.valueOf(organizationDto.getTip())));
            groupAttributes.put(Constants.KASPERSKY, Arrays.asList(String.valueOf(organizationDto.getKaspersky())));
            groupAttributes.put(Constants.DDP, Arrays.asList(String.valueOf(organizationDto.getDdp())));
            groupAttributes.put(Constants.COMMUNITY, Arrays.asList(String.valueOf(organizationDto.getCommunity())));
            groupAttributes.put(Constants.INFRA_MONITORING, Arrays.asList(String.valueOf(organizationDto.getInfraMonitoring())));
            groupAttributes.put(Constants.APT, Arrays.asList(String.valueOf(organizationDto.getApt())));
            groupAttributes.put(Constants.VP, Arrays.asList(String.valueOf(organizationDto.getVp())));
            groupAttributes.put(Constants.SIEM, Arrays.asList(String.valueOf(organizationDto.getSiem())));
            groupAttributes.put(Constants.SOUR, Arrays.asList(String.valueOf(organizationDto.getSour())));
            groupAttributes.put(Constants.SKURIO, Arrays.asList(String.valueOf(organizationDto.getSkurio())));
            groupAttributes.put(Constants.RISK_X_CHANGE, Arrays.asList(String.valueOf(organizationDto.getRiskXchange())));
            groupAttributes.put(Constants.CREATED_BY, Arrays.asList(createdBy));
            groupAttributes.put(Constants.CREATED_BY_COMPANY, Arrays.asList(org));
            groupAttributes.put(Constants.SOC_ASSIGNED, Arrays.asList("false"));

            if (organizationDto.getSocUserId() != null)
                groupAttributes.put(Constants.SOC_SUPPORT, Arrays.asList("true"));
            else
                groupAttributes.put(Constants.SOC_SUPPORT, Arrays.asList("false"));

            groupRepresentation.setAttributes(groupAttributes);

            Response groupResponse = groupsResource.add(groupRepresentation);

            if (groupResponse.getStatus() == HttpStatus.CREATED.value()) {
                responseDto.setStatusCode(HttpStatus.CREATED.value());
                responseDto.setResponseMsg("Organization Successfully Added");

                // Create Admin User Of Organization
                userService.signUp(organizationDto.getCreateRequestListDto(), createdBy, organizationDto.getCompanyName(), true);

                // Assign Soc To Organization
                if (organizationDto.getSocUserId() != null)
                    userUtil.assignSoc(organizationDto.getSocUserId(), organizationDto.getCompanyName(), realmResource);

            } else if (groupResponse.getStatus() == HttpStatus.CONFLICT.value()) {
                responseDto.setStatusCode(HttpStatus.CONFLICT.value());
                responseDto.setResponseMsg("Organization Already Exist");

            } else {
                responseDto.setStatusCode(groupResponse.getStatus());
                responseDto.setResponseMsg(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while adding organization: {}", e.getMessage());
            responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseDto.setResponseMsg(Constants.FAILURE);
        }
        return responseDto;
    }

    @Override
    public OrganizationViewDto getOrganization(String orgId, Boolean listing, GroupRepresentation groupRepresentation) {

        OrganizationViewDto organizationDto = null;
        GroupRepresentation group;
        try {
            if (!listing.booleanValue()) {
                Keycloak keycloak = keycloakUtil.getInstance();
                RealmResource realmResource = keycloak.realm(realm);

                GroupsResource groupsResource = realmResource.groups();
                GroupResource groupResource = groupsResource.group(orgId);
                group = groupResource.toRepresentation();
            } else
                group = groupRepresentation;

            if (group != null) {
                organizationDto = new OrganizationViewDto();
                Map<String, List<String>> attributes = group.getAttributes();

                organizationDto.setId(group.getId());
                organizationDto.setCompanyName(group.getName());
                organizationDto.setSubscriptionType(attributes.get(Constants.SUBSCRIPTION_TYPE).get(0));
                organizationDto.setSubscriptionStartDate(attributes.get(Constants.SUBSCRIPTION_START_DATE).get(0));
                organizationDto.setSubscriptionEndDate(attributes.get(Constants.SUBSCRIPTION_END_DATE).get(0));
                organizationDto.setSector(attributes.get(Constants.SECTOR).get(0));
                organizationDto.setOrganizationStatus(Boolean.valueOf(attributes.get(Constants.ORGANIZATION_STATUS).get(0)));

                UserRepresentation userRepresentation = userUtil.getUserByUsername(attributes.get(Constants.CREATED_BY).get(0));
                if (userRepresentation.isEnabled().booleanValue())
                    organizationDto.setAccountManager(userRepresentation.getAttributes().get(Constants.DISPLAY_NAME).get(0));

                // Get Org Base & Additional Roles
                setCompanyBaseAndAdditionalRole(organizationDto, attributes.get(Constants.COMPANY_ROLES).toString());

                // Get Assign Soc
                List<UserRepresentation> userRepresentations = userUtil.getUsersOfOrganization(group.getName());
                if (!userRepresentations.isEmpty()) {
                    for (UserRepresentation user : userRepresentations) {
                        String roles = userUtil.getUserRoles(user.getId()).toString();
                        if (roles.contains(Constants.SOC)) {
                            organizationDto.setAssignedSoc(user.getAttributes().get(Constants.DISPLAY_NAME).get(0));
                            organizationDto.setSocUserId(user.getId());
                            organizationDto.setSocUserEmailId(user.getEmail());
                            break;
                        }
                    }
                }

                // Get Org Admin User
                List<UserRepresentation> users = userUtil.getUsersOfOrganization(group.getName());
                if (!users.isEmpty()) {
                    for (UserRepresentation user : userRepresentations) {
                        List<String> roles = userUtil.getUserRoles(user.getId());
                        if (!roles.isEmpty()) {
                            for (String role : roles) {
                                if (role.contains(Constants.ADMIN) && !role.contains(Constants.SOC)) {
                                    organizationDto.setOrgAdminEmailId(user.getEmail());
                                    organizationDto.setOrgAdminRole(StringUtils.capitalize(Constants.ADMIN));
                                    break;
                                }
                            }
                        }
                    }
                }

                if (!listing.booleanValue()) {
                    organizationDto.setTip(Boolean.valueOf(attributes.get(Constants.TIP).get(0)));
                    organizationDto.setDdp(Boolean.valueOf(attributes.get(Constants.DDP).get(0)));
                    organizationDto.setCommunity(Boolean.valueOf(attributes.get(Constants.COMMUNITY).get(0)));
                    organizationDto.setInfraMonitoring(Boolean.valueOf(attributes.get(Constants.INFRA_MONITORING).get(0)));
                    organizationDto.setApt(Boolean.valueOf(attributes.get(Constants.APT).get(0)));
                    organizationDto.setVp(Boolean.valueOf(attributes.get(Constants.VP).get(0)));
                    organizationDto.setSiem(Boolean.valueOf(attributes.get(Constants.SIEM).get(0)));
                    organizationDto.setSour(Boolean.valueOf(attributes.get(Constants.SOUR).get(0)));
                    organizationDto.setSkurio(Boolean.valueOf(attributes.get(Constants.SKURIO).get(0)));
                    organizationDto.setRiskXchange(Boolean.valueOf(attributes.get(Constants.RISK_X_CHANGE).get(0)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting organization: {}", e.getMessage());
            organizationDto = null;
        }
        return organizationDto;
    }

    @Override
    public List<OrganizationViewDto> getOrganizations(String createdByOrg) {

        List<OrganizationViewDto> organizationDtos = null;
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);

            GroupsResource groupsResource = realmResource.groups();
            List<GroupRepresentation> groupRepresentations = groupsResource.groups();

            if (!groupRepresentations.isEmpty()) {
                organizationDtos = new ArrayList<>();

                for (GroupRepresentation groupRepresentation : groupRepresentations) {
                    GroupResource groupResource = groupsResource.group(groupRepresentation.getId());
                    GroupRepresentation group = groupResource.toRepresentation();
                    if (group.getAttributes().get(Constants.CREATED_BY_COMPANY).get(0).equals(createdByOrg)) {
                        OrganizationViewDto organizationViewDto = getOrganization(group.getId(), Boolean.TRUE, group);
                        if (organizationViewDto != null)
                            organizationDtos.add(organizationViewDto);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting organizations list: {}", e.getMessage());
        }
        return organizationDtos;
    }

    @Override
    public ResponseDto updateOrganization(OrganizationDto organizationDto, String userId) {

        ResponseDto responseDto = new ResponseDto();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            GroupsResource groupsResource = realmResource.groups();
            GroupResource groupResource = groupsResource.group(organizationDto.getOrgId());
            GroupRepresentation group = groupResource.toRepresentation();
            UserResource userResource = userUtil.getUser(userId);

            // Merged Company Additional Roles
            if (organizationDto.getAdditionalRoles() != null && !organizationDto.getAdditionalRoles().isEmpty())
                organizationDto.getCompanyRoles().addAll(organizationDto.getAdditionalRoles());

            // Check User Has Access To Add This Company Role
            if (!hasAccess(userResource, organizationDto.getCompanyRoles())) {
                responseDto.setStatusCode(HttpStatus.FORBIDDEN.value());
                responseDto.setResponseMsg("User Don't Have Access To Added This Company Role");
                return responseDto;
            }

            if (group != null) {
                Map<String, List<String>> attributes = group.getAttributes();
                group.setName(organizationDto.getCompanyName());

                attributes.put(Constants.SUBSCRIPTION_TYPE, Arrays.asList(SubscriptionTypeEnum.valueOf(organizationDto.getSubscriptionType()).name()));
                attributes.put(Constants.SUBSCRIPTION_END_DATE, Arrays.asList(organizationDto.getSubscriptionEndDate()));
                attributes.put(Constants.SECTOR, Arrays.asList(SectorEnum.valueOf(organizationDto.getSector()).name()));
                attributes.put(Constants.COMPANY_ROLES, organizationDto.getCompanyRoles());
                attributes.put(Constants.TIP, Arrays.asList(String.valueOf(organizationDto.getTip())));
                attributes.put(Constants.KASPERSKY, Arrays.asList(String.valueOf(organizationDto.getKaspersky())));
                attributes.put(Constants.DDP, Arrays.asList(String.valueOf(organizationDto.getDdp())));
                attributes.put(Constants.COMMUNITY, Arrays.asList(String.valueOf(organizationDto.getCommunity())));
                attributes.put(Constants.INFRA_MONITORING, Arrays.asList(String.valueOf(organizationDto.getInfraMonitoring())));
                attributes.put(Constants.APT, Arrays.asList(String.valueOf(organizationDto.getApt())));
                attributes.put(Constants.VP, Arrays.asList(String.valueOf(organizationDto.getVp())));
                attributes.put(Constants.SKURIO, Arrays.asList(String.valueOf(organizationDto.getSkurio())));
                attributes.put(Constants.RISK_X_CHANGE, Arrays.asList(String.valueOf(organizationDto.getRiskXchange())));

                if (organizationDto.getSocUserId() != null) {
                    boolean socExist = userUtil.assignSoc(organizationDto.getSocUserId(), organizationDto.getCompanyName(), realmResource);
                    if (socExist) {
                        responseDto.setStatusCode(HttpStatus.CONFLICT.value());
                        responseDto.setResponseMsg("Soc Already Assigned To this organization");
                    }
                }
                group.setAttributes(attributes);
                groupResource.update(group);
                responseDto.setStatusCode(HttpStatus.OK.value());
                responseDto.setResponseMsg("Organization Successfully Updated");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while updating organization: {}", e.getMessage());
            responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseDto.setResponseMsg(Constants.FAILURE);
        }
        return responseDto;
    }

    @Override
    public ResponseDto orgStatusUpdate(String orgId, Boolean status) {

        ResponseDto responseDto = new ResponseDto();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            GroupsResource groupsResource = realmResource.groups();
            GroupResource groupResource = groupsResource.group(orgId);
            GroupRepresentation group = userUtil.getGroupDetail(groupResource);

            if (group != null) {
                Map<String, List<String>> attributes = group.getAttributes();
                attributes.put(Constants.ORGANIZATION_STATUS, Arrays.asList(String.valueOf(status)));
                group.setAttributes(attributes);
                groupResource.update(group);
                responseDto.setStatusCode(HttpStatus.OK.value());
                if (status.booleanValue())
                    responseDto.setResponseMsg("Organization Enabled");
                else
                    responseDto.setResponseMsg("Organization Disabled");
            } else
                responseDto.setStatusCode(HttpStatus.NO_CONTENT.value());

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while disabling organization: {}", e.getMessage());
            responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseDto.setResponseMsg(Constants.FAILURE);
        }
        return responseDto;
    }

    @Override
    public ResponseDto unAssignSoc(String socUserId, String orgId, String userId) {

        ResponseDto responseDto = new ResponseDto();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            GroupsResource groupsResource = realmResource.groups();
            GroupResource groupResource = groupsResource.group(orgId);

            if (groupResource != null) {
                GroupRepresentation group = groupResource.toRepresentation();
                log.info("Group name: {}", group.getName());

                UserResource loggedInUserResource = userUtil.getUser(userId);
                UserRepresentation loggedInUser = loggedInUserResource.toRepresentation();
                List<String> roles = userUtil.getUserRoles(loggedInUserResource.roles());

                UserResource userResource = userUtil.getUser(socUserId);

                if (roles.toString().contains(Constants.ANALYST)) {
                    String createdBy = userResource.toRepresentation().getAttributes().get(Constants.CREATED_BY).get(0);

                    if (!loggedInUser.getUsername().equals(createdBy)) {
                        responseDto.setStatusCode(HttpStatus.FORBIDDEN.value());
                        responseDto.setResponseMsg("Cannot Un-Assign Soc Assigned By Other User");
                        return responseDto;
                    }
                }
                // Un-assign soc user from organization
                userResource.leaveGroup(group.getId());
                userResource.update(userResource.toRepresentation());

                // Set assigned soc attribute false if no-soc left for organization
                userUtil.updateSocAssignedAttribute(socUserId, userResource.toRepresentation().getAttributes().get(Constants.OWN_COMPANY).get(0),
                        realmResource);

                responseDto.setStatusCode(HttpStatus.OK.value());
                responseDto.setResponseMsg("Soc User Un-Assigned From Organization: " + group.getName());
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while un-assigning soc user: {}", e.getMessage());
            responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseDto.setResponseMsg(Constants.FAILURE);
        }
        return responseDto;
    }

    private boolean hasAccess(UserResource userResource, List<String> companyRoles) {

        boolean hasAccess = false;
        String role = null;
        String userRoles = userUtil.getUserRoles(userResource.roles()).toString();

        if (userRoles.contains(Constants.MASTER))
            role = Constants.MASTER;
        else if (userRoles.contains(Constants.DISTRIBUTOR))
            role = Constants.DISTRIBUTOR;
        else if (userRoles.contains(Constants.PARTNER))
            role = Constants.PARTNER;

        if (role != null) {
            switch (role) {
                case Constants.MASTER:
                    if (companyRoles.contains(StringUtils.capitalize(Constants.DISTRIBUTOR))
                            || companyRoles.contains(StringUtils.capitalize(Constants.PARTNER))
                            || companyRoles.contains(StringUtils.capitalize(Constants.CLIENT)))
                        hasAccess = true;
                    break;
                case Constants.DISTRIBUTOR:
                    if (companyRoles.contains(StringUtils.capitalize(Constants.PARTNER))
                            || companyRoles.contains(StringUtils.capitalize(Constants.CLIENT)))
                        hasAccess = true;
                    break;
                case Constants.PARTNER:
                    if (companyRoles.contains(StringUtils.capitalize(Constants.CLIENT)))
                        hasAccess = true;
                    break;
                default:
                    log.info("No case matches for company role access");
            }
        }
        return hasAccess;
    }

    private void setCompanyBaseAndAdditionalRole(OrganizationViewDto organizationDto, String companyRoles) {

        String[] roles = companyRoles.replace("[", "").replace("]", "").split(",");
        organizationDto.setCompanyAdditionalRoles(new ArrayList<>());

        for (String role : roles) {
            role = role.trim();

            if (companyRoles.toLowerCase().contains(Constants.ADMIN)) {
                if (role.equalsIgnoreCase(Constants.ADMIN))
                    organizationDto.setCompanyBaseRole(role);
                else
                    organizationDto.getCompanyAdditionalRoles().add(role);

            } else if (companyRoles.toLowerCase().contains(Constants.DISTRIBUTOR)) {
                if (role.equalsIgnoreCase(Constants.DISTRIBUTOR))
                    organizationDto.setCompanyBaseRole(role);
                else
                    organizationDto.getCompanyAdditionalRoles().add(role);

            } else if (companyRoles.toLowerCase().contains(Constants.PARTNER)) {
                if (role.equalsIgnoreCase(Constants.PARTNER))
                    organizationDto.setCompanyBaseRole(role);
                else
                    organizationDto.getCompanyAdditionalRoles().add(role);

            } else if (companyRoles.toLowerCase().contains(Constants.CLIENT) && role.equalsIgnoreCase(Constants.CLIENT))
                organizationDto.setCompanyBaseRole(role);
        }
    }
}
