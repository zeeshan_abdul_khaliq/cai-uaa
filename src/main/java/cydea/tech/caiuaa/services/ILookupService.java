package cydea.tech.caiuaa.services;

import cydea.tech.caiuaa.dtos.UserDto;

import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
public interface ILookupService {

    List<UserDto> getSocUsers(String userId, String org);

    List<String> getSubscriptionTypes();

    List<String> getCompanyRoles(String userId);

    List<String> getAdditionalRoles(String userId);

    List<String> getSocAdditionalRole(String userId);

    List<String> getSectors();

    List<String> getAccessRoles(String userId, Boolean soc);
}
