package cydea.tech.caiuaa.services;

import cydea.tech.caiuaa.dtos.UserRegistrationAndUpdateDto;

/**
 * @author Muhammad Nabeel Rashid
 */
public interface IEmailService {

    void sendMailForUser(String instance, UserRegistrationAndUpdateDto userRegistrationAndUpdateDto);
}