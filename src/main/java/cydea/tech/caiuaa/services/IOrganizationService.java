package cydea.tech.caiuaa.services;

import cydea.tech.caiuaa.dtos.OrganizationDto;
import cydea.tech.caiuaa.dtos.OrganizationViewDto;
import cydea.tech.caiuaa.dtos.ResponseDto;
import org.keycloak.representations.idm.GroupRepresentation;

import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
public interface IOrganizationService {

    ResponseDto signUp(OrganizationDto organizationDto, String userId, String createdBy, String org);

    OrganizationViewDto getOrganization(String orgId, Boolean listing, GroupRepresentation groupRepresentation);

    List<OrganizationViewDto> getOrganizations(String createdByOrg);

    ResponseDto updateOrganization(OrganizationDto organizationDto, String userId);

    ResponseDto orgStatusUpdate(String orgId, Boolean status);

    ResponseDto unAssignSoc(String socUserId, String orgId, String userId);
}
