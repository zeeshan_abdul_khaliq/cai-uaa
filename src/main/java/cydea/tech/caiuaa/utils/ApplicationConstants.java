package cydea.tech.caiuaa.utils;

public class ApplicationConstants {

    private ApplicationConstants() {
    }

    public static final String FAILURE = "your request cannot be processed.please try after some time";
    public static final String PASSWORD_MISMATCH = "Old password mismatch";
    public static final String PASSWORD_CHANGED_SUCCESSFULLY = "Password changed successfully";
    public static final String MESSAGE_CHANGE_PASSWORD_REQUEST = "Forgot password request";
    public static final String FAILURE_USER_DOES_NOT_EXIST = "user doesn't exits";
    public static final String USER_STATUS_CHANGED = "user status changed";
}
