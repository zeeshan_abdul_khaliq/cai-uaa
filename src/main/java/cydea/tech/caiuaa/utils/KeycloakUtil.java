package cydea.tech.caiuaa.utils;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Muhammad Mujtaba
 */
@Slf4j
@Component
public class KeycloakUtil {

    @Value("${keycloak.url}")
    private String url;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.secret}")
    private String secret;

    @Value("${keycloak.client}")
    private String client;

    @Value("${keycloak.admin.user}")
    private String userName;

    @Value("${keycloak.admin.password}")
    private String userPassword;

    private Keycloak instance;

    public Keycloak getInstance() {
        if (instance == null)
            instance = keycloak();
        return instance;
    }

    private Keycloak keycloak() {
        return KeycloakBuilder.builder().serverUrl(url).realm(realm).username(userName).password(userPassword)
                .clientId(client).clientSecret(secret).build();
    }
}
