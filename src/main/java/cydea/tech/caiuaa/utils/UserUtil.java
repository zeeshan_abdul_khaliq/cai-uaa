package cydea.tech.caiuaa.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import cydea.tech.caiuaa.dtos.ResponseDto;
import cydea.tech.caiuaa.dtos.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.ws.rs.NotFoundException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Muhammad Mujtaba
 */
@Slf4j
@Component
public class UserUtil {

    @Value("${keycloak.realm}")
    private String realm;

    private KeycloakUtil keycloakUtil;

    public UserUtil(KeycloakUtil keycloakUtil) {
        this.keycloakUtil = keycloakUtil;
    }

    public ResponseDto checkUserAccess(RealmResource realmResource, String username, List<String> roles) {

        StringBuilder appendedRoles = new StringBuilder();
        UsersResource usersResource = realmResource.users();
        UserResource user = usersResource.get(usersResource.search(username).get(0).getId());

        user.roles().realmLevel().listEffective().forEach(r -> {
            RoleRepresentation realmRole = realmResource.roles().get(r.getName()).toRepresentation();
            if (realmRole.getDescription() != null)
                appendedRoles.append(realmRole.getDescription()).append(',');
        });

        if (appendedRoles.length() > 0) {
            String accessRoles = appendedRoles.toString();
            String[] rolesArray = accessRoles.split(",");

            boolean hasAccess = true;
            for (String ur : roles) {
                if (!ur.equals(Constants.DEFAULT_ROLE_CAI_UAA)) {
                    hasAccess = Arrays.stream(rolesArray).anyMatch(m -> (m.equals(ur)));
                    if (!hasAccess)
                        break;
                }
            }
            if (hasAccess)
                return new ResponseDto(HttpStatus.OK.value(), "Has Access");
        }
        return new ResponseDto(HttpStatus.FORBIDDEN.value(), "Forbidden");
    }

    public List<String> getUserRoles(String userId) {

        List<String> roles = new ArrayList<>();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);

            UsersResource usersResource = realmResource.users();
            UserResource user = usersResource.get(userId);
            user.roles().realmLevel().listEffective().forEach(role -> {
                if (!role.getName().equals(Constants.DEFAULT_ROLE_CAI_UAA))
                    roles.add(role.getName());
            });

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while get user roles: {}", e.getMessage());
        }
        return roles;
    }

    public List<String> getUserGroups(RealmResource realmResource, String userId) {

        List<String> groups = new ArrayList<>();
        try {
            UsersResource usersResource = realmResource.users();
            UserResource user = usersResource.get(userId);
            groups = user.groups().stream().map(GroupRepresentation::getName).collect(Collectors.toList());

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while get user groups: {}", e.getMessage());
        }
        return groups;
    }

    public List<String> getUserRoles(RoleMappingResource resource) {

        List<String> roles = new ArrayList<>();
        try {
                resource.realmLevel().listEffective().forEach(role -> {
                if (!role.getName().equals(Constants.DEFAULT_ROLE_CAI_UAA))
                    roles.add(role.getName());
            });

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while get user roles: {}", e.getMessage());
        }
        return roles;
    }

    public List<UserDto> getUsersInRole(String role, String org) {

        List<UserDto> userDtos = new ArrayList<>();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            Set<UserRepresentation> userRepresentations = realmResource.roles().get(role).getRoleUserMembers();

            for (UserRepresentation user : userRepresentations) {
                if (user.isEnabled().booleanValue() && getUserGroups(realmResource, user.getId()).contains(org)) {
                    UserDto userDto = new UserDto();
                    userDto.setUserId(user.getId());
                    userDto.setEmailId(user.getEmail());
                    userDtos.add(userDto);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while get users in role: {}", e.getMessage());
        }
        return userDtos;
    }

    public List<String> getAccessRoles(String userId, Boolean soc) {

        List<String> roles = new ArrayList<>();
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);

            StringBuilder appendedRoles = new StringBuilder();
            UsersResource usersResource = realmResource.users();
            UserResource user = usersResource.get(userId);

            user.roles().realmLevel().listEffective().forEach(r -> {
                if (!r.getName().equals(Constants.DEFAULT_ROLE_CAI_UAA)) {
                    RoleRepresentation realmRole = realmResource.roles().get(r.getName()).toRepresentation();
                    if (realmRole.getDescription() != null)
                        appendedRoles.append(realmRole.getDescription()).append(',');
                }
            });

            if (appendedRoles.length() > 0) {
                String accessRoles = appendedRoles.toString();
                String[] rolesArray = accessRoles.split(",");
                String baseRole = getUserBaseRole(user);

                for (String role : rolesArray) {
                    if (soc.booleanValue() && role.contains(baseRole) && role.contains(Constants.SOC)) {
                        roles.add(WordUtils.capitalize(role.replace("-", " ")));
                        break;

                    } else if (!soc.booleanValue() && !role.contains(Constants.SOC)
                            && baseRole.concat("-").concat(Constants.ANALYST).equals(role)) {
                        roles.add(WordUtils.capitalize(role.replace("-", " ")));
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred while get user access roles: {}", e.getMessage());
        }
        return roles;
    }

    public UserResource getUser(String userId) {

        UserResource userResource = null;
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            // Get realm users
            UsersResource usersResource = realmResource.users();
            // Get user
            userResource = usersResource.get(userId);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting users: {}", e.getMessage());
        }
        return userResource;
    }

    public UserRepresentation getUserByUsername(String username) {

        UserRepresentation user = null;
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            // Get realm users
            UsersResource usersResource = realmResource.users();
            // Get user
            List<UserRepresentation> userRepresentations = usersResource.search(username);
            if (!userRepresentations.isEmpty())
                user = userRepresentations.get(0);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting users: {}", e.getMessage());
        }
        return user;
    }

    public List<UserRepresentation> getUsersOfOrganization(String org) {

        List<UserRepresentation> userRepresentations = null;
        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);

            GroupsResource groupsResource = realmResource.groups();
            List<GroupRepresentation> groupRepresentations = groupsResource.groups(org, 0, 1);
            GroupResource groupResource = groupsResource.group(groupRepresentations.get(0).getId());
            userRepresentations = groupResource.members();

        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting members of org: {}", e.getMessage());
        }
        return userRepresentations;
    }

    public String getUserNameFromToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        Claim username = jwt.getClaim("preferred_username");
        return username.asString();
    }

    public String getOrganizationFromToken(String token) {

        DecodedJWT jwt = JWT.decode(token);
        Claim organization = jwt.getClaim(Constants.ORGANIZATION);
        List<String> organizationList = organization.asList(String.class);
        return organizationList.get(0);
    }

    public UserRepresentation getUserDetail(UserResource userResource) {
        try {
            return userResource.toRepresentation();
        } catch (NotFoundException e) {
            log.error("User not found: {}", e.getMessage());
            return null;
        }
    }

    public GroupRepresentation getGroupDetail(GroupResource groupResource) {
        try {
            return groupResource.toRepresentation();
        } catch (NotFoundException e) {
            log.error("Group not found: {}", e.getMessage());
            return null;
        }
    }

    public List<String> transformRoles(List<String> roles) {
        List<String> transformRoles = new ArrayList<>();
        roles.forEach(role -> transformRoles.add(role.replace(" ", "-").toLowerCase()));
        return transformRoles;
    }

    public List<String> transformRolesFromCompanyRoles(List<String> companyRoles, String userRole) {
        List<String> transformRoles = new ArrayList<>();
        companyRoles.forEach(companyRole -> transformRoles.add(companyRole.concat("-").concat(userRole).toLowerCase()));
        return transformRoles;
    }

    public List<String> transformRolesFromAdditionalRoles(List<String> additionalRoles, List<String> userRoles) {

        List<String> transformRoles = new ArrayList<>();
        if (userRoles.toString().contains(Constants.SOC) || userRoles.toString().contains(Constants.ANALYST))
            additionalRoles.forEach(additionalRole -> transformRoles.add(additionalRole.concat("-").concat(Constants.ANALYST).toLowerCase()));
        return transformRoles;
    }

    public String getUserBaseRole(UserResource userResource) {

        String role = null;
        UserRepresentation user = getUserDetail(userResource);

        if (user != null) {
            String userRoles = getUserRoles(userResource.roles()).toString();

            if (userRoles.contains(Constants.MASTER))
                role = Constants.MASTER;
            else if (userRoles.contains(Constants.DISTRIBUTOR))
                role = Constants.DISTRIBUTOR;
            else if (userRoles.contains(Constants.PARTNER))
                role = Constants.PARTNER;
            else if (userRoles.contains(Constants.CLIENT))
                role = Constants.CLIENT;
        }
        return role;
    }

    public String getOrganizationBaseRole(List<String> roles) {

        String role = null;
        List<String> companyRoles = new ArrayList<>();
        roles.forEach(r -> companyRoles.add(r.toLowerCase()));

        if (companyRoles.contains(Constants.ADMIN))
            role = Constants.ADMIN;
        else if (companyRoles.contains(Constants.DISTRIBUTOR))
            role = Constants.DISTRIBUTOR;
        else if (companyRoles.contains(Constants.PARTNER))
            role = Constants.PARTNER;
        else if (companyRoles.contains(Constants.CLIENT))
            role = Constants.CLIENT;

        return StringUtils.capitalize(role);
    }

    public String getUserBaseRole(List<String> roles) {

        for (String role : roles) {
            if (role.contains(Constants.MASTER))
                return WordUtils.capitalize(role.replace("-", " "));
            else if (role.contains(Constants.DISTRIBUTOR) && !roles.toString().contains(Constants.MASTER))
                return WordUtils.capitalize(role.replace("-", " "));
            else if (role.contains(Constants.PARTNER) && !roles.toString().contains(Constants.MASTER)
                    && !roles.toString().contains(Constants.DISTRIBUTOR))
                return WordUtils.capitalize(role.replace("-", " "));
            else if (role.contains(Constants.CLIENT) && !roles.toString().contains(Constants.MASTER)
                    && !roles.toString().contains(Constants.DISTRIBUTOR) && !roles.toString().contains(Constants.PARTNER))
                return WordUtils.capitalize(role.replace("-", " "));
        }
        return null;
    }

    public List<String> getUserAdditionalRoles(List<String> roles, String baseRole) {

        List<String> additionalRoles = new ArrayList<>();
        for (String role : roles) {
            if (!role.contains(baseRole))
                additionalRoles.add(WordUtils.capitalize(role.split("-")[0]));
        }
        return additionalRoles;
    }

    public List<String> getRolesFromToken(String token) {

        List<String> normalRoles = new ArrayList<>();
        try {
            DecodedJWT jwt = JWT.decode(token);
            Claim roles = jwt.getClaim(Constants.REALM_ACCESS);
            Map<String, Object> roleMap = roles.asMap();
            List<String> list = (List<String>) roleMap.get(Constants.USER_ROLE);
            normalRoles = list.stream().filter(x -> !x.equals(Constants.DEFAULT_ROLE_CAI_UAA)).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return normalRoles;
    }

    @Async
    public void assignUnAssignedOrgsToSocAdmin(UserResource userResource, String userOrg, RealmResource realmResource) {

        try {
            GroupsResource groupsResource = realmResource.groups();
            List<GroupRepresentation> groupRepresentations = groupsResource.groups();

            for (GroupRepresentation groupRepresentation : groupRepresentations) {
                GroupResource group = groupsResource.group(groupRepresentation.getId());
                Map<String, List<String>> attributes = group.toRepresentation().getAttributes();

                if (attributes.get(Constants.CREATED_BY_COMPANY).get(0).equals(userOrg)
                        && Boolean.parseBoolean(attributes.get(Constants.SOC_SUPPORT).get(0))
                        && !Boolean.parseBoolean(attributes.get(Constants.SOC_ASSIGNED).get(0)))
                    assignSoc(userResource.toRepresentation().getId(), groupRepresentation.getName(), realmResource);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void assignSocAnalystOrgsToSocAdmin(String userId, String userOrg) {

        try {
            Keycloak keycloak = keycloakUtil.getInstance();
            RealmResource realmResource = keycloak.realm(realm);
            String socAdminId = null;

            List<UserRepresentation> userRepresentations = getUsersOfOrganization(userOrg);
            for (UserRepresentation user : userRepresentations) {

                List<String> roles = getUserRoles(user.getId());
                if (roles.toString().contains(Constants.SOC_ADMIN)) {
                    socAdminId = user.getId();
                    break;
                }
            }
            List<String> organizations = getUserGroups(realmResource, userId);
            if (!organizations.isEmpty()) {
                for (String org : organizations) {
                    if (socAdminId != null)
                        assignSoc(socAdminId, org, realmResource);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean assignSoc(String socUserId, String org, RealmResource realmResource) {

        boolean socExists = false;
        GroupsResource groupsResource = realmResource.groups();
        List<GroupRepresentation> groupRepresentations = groupsResource.groups(org, 0, 1);

        if (groupRepresentations != null && !groupRepresentations.isEmpty()) {
            GroupRepresentation groupRepresentation = groupRepresentations.get(0);
            log.info("Group name: {}", groupRepresentation.getName());

            List<UserRepresentation> userRepresentations = getUsersOfOrganization(org);
            for (UserRepresentation user : userRepresentations) {
                List<String> userRoles = getUserRoles(user.getId());
                if (user.isEnabled().booleanValue() && userRoles.toString().contains(Constants.SOC))
                    socExists = true;
            }

            if (!socExists) {
                // Get soc user & assign organization
                UserResource userResource = getUser(socUserId);
                userResource.joinGroup(groupRepresentation.getId());
                userResource.update(userResource.toRepresentation());

                GroupResource groupResource = groupsResource.group(groupRepresentation.getId());
                GroupRepresentation group = groupResource.toRepresentation();
                if (!Boolean.parseBoolean(group.getAttributes().get(Constants.SOC_ASSIGNED).get(0))) {
                    group.getAttributes().put(Constants.SOC_ASSIGNED, Arrays.asList("true"));
                    groupResource.update(group);
                }
            }
        }
        return socExists;
    }

    @Async
    public void updateSocAssignedAttribute(String userId, String userOrg, RealmResource realmResource) {

        GroupsResource groupsResource = realmResource.groups();
        List<String> groups = getUserGroups(realmResource, userId);

        for (String group : groups) {
            GroupResource groupResource = groupsResource.group(groupsResource.groups(group, 0, 1).get(0).getId());
            GroupRepresentation groupRepresentation = groupResource.toRepresentation();

            if (!userOrg.equals(group) && Boolean.parseBoolean(groupRepresentation.getAttributes().get(Constants.SOC_ASSIGNED).get(0))) {
                groupRepresentation.getAttributes().put(Constants.SOC_ASSIGNED, Arrays.asList("false"));
                groupResource.update(groupRepresentation);
            }
        }
    }
}
