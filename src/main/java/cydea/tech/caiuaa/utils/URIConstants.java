package cydea.tech.caiuaa.utils;

/**
 * @author Muhammad Mujtaba ,Muhammad Nabeel Rashid
 */
public final class URIConstants {

    private URIConstants() {
    }

    public static final String RESEND_LINK = "/resend-link";
    public static final String RE_INVITE_USER = "/re-invite-user";
    public static final String LINK_STATUS = "/link-status";
    public static final String UAA = "/uaa";
    public static final String ADD_ORGANIZATION = "/add-organization";
    public static final String GET_ORGANIZATION = "/get-organization";
    public static final String GET_ORGANIZATIONS_LIST = "/get-organizations-list";
    public static final String UPDATE_ORGANIZATION = "/update-organization";
    public static final String ORG_STATUS_UPDATE = "/org-status-update";
    public static final String ADD_USER = "/add-user";
    public static final String UPDATE_USER = "/update-user";
    public static final String GET_USER = "/get-user";
    public static final String GET_SOC_USER = "/get-soc-user";
    public static final String GET_USERS_LIST = "/get-users-list";
    public static final String DELETE_USER = "/delete-user";
    public static final String UPDATE_PROFILE = "/update-profile";
    public static final String GET_TOKEN = "/get-token";
    public static final String GET_SOC_USERS = "/get-soc-users";
    public static final String SUBSCRIPTION_TYPES = "/subscription-types";
    public static final String COMPANY_ROLES = "/company-roles";
    public static final String ADDITIONAL_ROLES = "/additional-roles";
    public static final String SOC_ADDITIONAL_ROLE = "/soc-additional-role";
    public static final String SECTORS = "/sectors";
    public static final String ACCESS_ROLES = "/access-roles";
    public static final String FORGOT_PASSWORD = "/forgot-password";
    public static final String CHANGE_PASSWORD = "/change-password";
    public static final String UPDATE_PASSWORD = "/update-password";
    public static final String ENABLE_DISABLE_USER = "enable-user";
    public static final String GET_CLIENT_ADMIN_EMAIL = "/get-client-admin-email";
    public static final String UN_ASSIGN_SOC = "/un-assign-soc";
    public static final String CREATED_BY = "/created-by";
    public static final String UN_ASSIGNED_SOC_ORGANIZATION = "/un-assigned-soc-organizations";

    public static final String BASE_NOTIFICATION = "/notification";
    public static final String NOTIFICATION_REGISTRATION = "/register-user";
}
