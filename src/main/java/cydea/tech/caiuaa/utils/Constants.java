package cydea.tech.caiuaa.utils;

/**
 * @author Muhammad Mujtaba,Nabeel Rashid
 */
public final class Constants {

    private Constants() {
    }

    public static final String CLIENT_ID = "cai-uaa";
    public static final String CREATED_BY = "createdBy";
    public static final String REGISTRATION = "Registration";
    public static final String NAME = "name";
    public static final String CONTENT_ID = "Content-ID";
    public static final String BUTTON = "button";
    public static final String HEADER = "header";
    public static final String BODY1 = "body1";
    public static final String BODY2 = "body2";
    public static final String USER_REGISTRATION = "User Registration";
    public static final String PASSWORD_RESET = "Password Reset";
    public static final String USER_REGISTRATION_BODY1 = "You have been invited to use Cydea Adaptive Intelligence Solutions.";
    public static final String USER_REGISTRATION_BODY2 = "Click on the button or link below to register for the platform.";
    public static final String PASSWORD_RESET_BODY1 = "A request to reset your password for your CAI account was submitted." +
            "Click on the button or link below to change your password.";
    public static final String PASSWORD_RESET_BODY2 = "If you did not make this request, just ignore this email.";
    public static final String LINK = "link";
    public static final String USER_CRUD = "userCrud";
    public static final String FORGOT_PASSWORD = "forgotPassword";
    public static final String CREATED_BY_COMPANY = "createdByCompany";
    public static final String OWN_COMPANY = "ownCompany";
    public static final String STATUS = "status";
    public static final String DISPLAY_NAME = "displayName";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String FAILURE = "Failure";
    public static final String SUBSCRIPTION_TYPE = "subscriptionType";
    public static final String COMPANY_ROLES = "companyRoles";
    public static final String SECTOR = "sector";
    public static final String SUBSCRIPTION_START_DATE = "subscriptionStartDate";
    public static final String SUBSCRIPTION_END_DATE = "subscriptionEndDate";
    public static final String ORGANIZATION_STATUS = "organizationStatus";
    public static final String TIP = "tip";
    public static final String KASPERSKY = "kaspersky";
    public static final String DDP = "ddp";
    public static final String COMMUNITY = "community";
    public static final String INFRA_MONITORING = "infraMonitoring";
    public static final String APT = "apt";
    public static final String VP = "vp";
    public static final String SIEM = "siem";
    public static final String SOUR = "sour";
    public static final String SKURIO = "skurio";
    public static final String RISK_X_CHANGE = "riskXchange";
    public static final String REALM_ACCESS = "realm_access";
    public static final String USER_ROLE = "roles";
    public static final String ORGANIZATION = "org";

    //Roles
    public static final String DEFAULT_ROLE_CAI_UAA = "default-roles-cai-uaa";
    public static final String MASTER = "master";
    public static final String MASTER_ADMIN = "master-admin";
    public static final String MASTER_ANALYST = "master-analyst";
    public static final String DISTRIBUTOR = "distributor";
    public static final String DISTRIBUTOR_ADMIN = "distributor-admin";
    public static final String DISTRIBUTOR_ANALYST = "distributor-analyst";
    public static final String PARTNER = "partner";
    public static final String PARTNER_ADMIN = "partner-admin";
    public static final String PARTNER_ANALYST = "partner-analyst";
    public static final String CLIENT_ADMIN = "client-admin";
    public static final String CLIENT_ANALYST = "client-analyst";
    public static final String CLIENT = "client";
    public static final String MASTER_SOC_ADMIN = "master-soc-admin";
    public static final String MASTER_SOC_ANALYST = "master-soc-analyst";
    public static final String DISTRIBUTOR_SOC_ADMIN = "distributor-soc-admin";
    public static final String DISTRIBUTOR_SOC_ANALYST = "distributor-soc-analyst";
    public static final String PARTNER_SOC_ADMIN = "partner-soc-admin";
    public static final String PARTNER_SOC_ANALYST = "partner-soc-analyst";
    public static final String ADMIN = "admin";
    public static final String ANALYST = "analyst";
    public static final String SOC = "soc";
    public static final String SOC_ADMIN = "soc-admin";
    public static final String SOC_ANALYST = "soc-analyst";
    public static final String SOC_ASSIGNED = "socAssigned";
    public static final String SOC_SUPPORT = "socSupport";
}
