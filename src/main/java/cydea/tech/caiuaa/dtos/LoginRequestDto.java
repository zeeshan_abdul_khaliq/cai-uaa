package cydea.tech.caiuaa.dtos;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequestDto implements Serializable {

    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
}
