package cydea.tech.caiuaa.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationDto implements Serializable {

    private String orgId;
    @NotEmpty
    private String companyName;
    @NotEmpty
    private String subscriptionType;
    @NotEmpty
    private String sector;
    @NotEmpty
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String subscriptionStartDate;
    @NotEmpty
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String subscriptionEndDate;
    @NotEmpty
    private List<String> companyRoles;
    private List<String> additionalRoles;

    private String socUserId;

    private Boolean organizationStatus;
    private Boolean tip;
    private Boolean kaspersky;
    private Boolean ddp;
    private Boolean community;
    private Boolean infraMonitoring;
    private Boolean apt;
    private Boolean vp;
    private Boolean siem;
    private Boolean sour;
    private Boolean skurio;
    private Boolean riskXchange;

    private UserCreateRequestListDto createRequestListDto;
}
