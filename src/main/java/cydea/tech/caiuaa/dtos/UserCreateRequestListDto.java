package cydea.tech.caiuaa.dtos;

import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateRequestListDto implements Serializable {

    private List<UserCreateRequestDto> userCreateRequestDtos;
}
