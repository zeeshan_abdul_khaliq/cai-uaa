package cydea.tech.caiuaa.dtos;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ForgotPasswordLinkDto {

	@NotEmpty
	private String Link;
	@NotEmpty
	private String updatedPassword;

}