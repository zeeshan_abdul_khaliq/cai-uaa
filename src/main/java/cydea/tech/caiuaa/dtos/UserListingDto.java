package cydea.tech.caiuaa.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserListingDto implements Serializable {

    private OrganizationUserListDto organizationUserListDto;
    private SocUserListDto socUserListDto;
    private boolean soc;
}
