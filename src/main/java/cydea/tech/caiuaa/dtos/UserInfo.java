package cydea.tech.caiuaa.dtos;

import lombok.Data;
import java.io.Serializable;

/**
 * @author Muhammad Mujtaba
 */
@Data
public class UserInfo implements Serializable {

    private String displayName;
    private String emailId;
}
