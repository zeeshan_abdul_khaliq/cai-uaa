package cydea.tech.caiuaa.dtos;

import lombok.*;

import java.io.Serializable;

/**
 * @author Muhammad Mujtaba
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class LoginResponseDto implements Serializable {

    private int responseCode;
    private String responseMessage;
    private LoginDto loginDto;
}
