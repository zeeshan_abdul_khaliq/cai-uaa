package cydea.tech.caiuaa.dtos;

import lombok.*;

import java.io.Serializable;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LoginDto implements Serializable {

    private String username;
    private String organizationName;
    private String role;
    private String accessToken;
    private Long expiresIn;
    private Long refreshExpiresIn;
    private String refreshToken;
}
