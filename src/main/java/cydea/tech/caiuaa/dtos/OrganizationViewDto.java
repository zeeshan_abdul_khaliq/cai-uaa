package cydea.tech.caiuaa.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationViewDto implements Serializable {

    private String id;
    private String companyName;
    private String subscriptionType;
    private String sector;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String subscriptionStartDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String subscriptionEndDate;

    private String companyBaseRole;
    private List<String> companyAdditionalRoles;

    private Boolean organizationStatus;
    private Boolean tip;
    private Boolean ddp;
    private Boolean community;
    private Boolean infraMonitoring;
    private Boolean apt;
    private Boolean vp;
    private Boolean siem;
    private Boolean sour;
    private Boolean skurio;
    private Boolean riskXchange;

    private String accountManager;
    private String orgAdminEmailId;
    private String orgAdminRole;
    private String assignedSoc;
    private String socUserId;
    private String socUserEmailId;
}
