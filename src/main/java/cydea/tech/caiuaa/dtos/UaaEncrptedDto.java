package cydea.tech.caiuaa.dtos;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UaaEncrptedDto {
	private String userId;
	private LocalDateTime localDateTime;

}
