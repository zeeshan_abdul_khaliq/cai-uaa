package cydea.tech.caiuaa.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Mutaba
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationUserListDto implements Serializable {

    private List<UserDto> activeUsers;
    private List<UserDto> disabledUsers;
    private List<UserDto> invitedUsers;
}
