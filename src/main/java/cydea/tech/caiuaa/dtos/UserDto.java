package cydea.tech.caiuaa.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto implements Serializable {

    private String userId;
    private String emailId;
    private String username;
    private String displayName;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String role;
    private List<String> additionalRoles;
    private List<OrganizationViewDto> organizationViewDtos;
}
