package cydea.tech.caiuaa.dtos;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Nabeel Rashid
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateRequestDto implements Serializable {

    @NotEmpty
    private String userId;
    @NotEmpty
    private String displayName;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    private String phoneNumber;
    @NotEmpty
    private List<String> roles;

    private List<String> additionalRoles;
    private String password;
    private Boolean enabled;
}
