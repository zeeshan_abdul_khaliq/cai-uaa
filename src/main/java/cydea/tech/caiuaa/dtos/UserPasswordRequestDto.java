package cydea.tech.caiuaa.dtos;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserPasswordRequestDto {
	
	@NotEmpty
	private String password;
	@NotEmpty
	private String oldPassword;
}