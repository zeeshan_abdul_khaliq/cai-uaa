package cydea.tech.caiuaa.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyRoleDto implements Serializable {

    private List<String> companyRoles;
}
