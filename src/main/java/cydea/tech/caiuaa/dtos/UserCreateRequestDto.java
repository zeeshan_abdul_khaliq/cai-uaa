package cydea.tech.caiuaa.dtos;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateRequestDto implements Serializable {

    @NotEmpty
    private String username;
    @Email
    @NotEmpty
    private String email;
    @NotEmpty
    private String companyName;
    @NotEmpty
    private String phoneNumber;
    @NotEmpty
    private String role;

    private List<String> roles;
    private List<String> additionalRoles;
}
