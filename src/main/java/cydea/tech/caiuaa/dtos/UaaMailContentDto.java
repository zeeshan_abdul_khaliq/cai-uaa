package cydea.tech.caiuaa.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Muhammad Nabeel Rashid
 */
@Getter
@Setter
@ToString
public class UaaMailContentDto implements Serializable {
    private String userID;
    private String username;
    private String emailId;
    private String organization;
    private String instance;
    private String subject;
    private String content;
    private String link;
}
