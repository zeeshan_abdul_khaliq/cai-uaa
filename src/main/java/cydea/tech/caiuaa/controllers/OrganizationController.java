package cydea.tech.caiuaa.controllers;

import cydea.tech.caiuaa.dtos.OrganizationDto;
import cydea.tech.caiuaa.dtos.OrganizationViewDto;
import cydea.tech.caiuaa.dtos.ResponseDto;
import cydea.tech.caiuaa.services.IOrganizationService;
import cydea.tech.caiuaa.utils.URIConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Muhammad Mujtaba, Muhammad Nabeel Rashid
 */
@Slf4j
@Validated
@RestController
@RequestMapping(URIConstants.UAA)
public class OrganizationController {

    private IOrganizationService iOrganizationService;

    public OrganizationController(IOrganizationService iOrganizationService) {
        this.iOrganizationService = iOrganizationService;
    }

    @PostMapping(URIConstants.ADD_ORGANIZATION)
    public ResponseEntity<ResponseDto> addOrganization(@RequestBody @Valid OrganizationDto organizationDto,
                                                       @RequestHeader("userId") String userId,
                                                       @RequestHeader("username") String createdBy, @RequestHeader("org") String org) {
        log.info("Request receive to add organization: {}", organizationDto.getCompanyName());
        ResponseDto responseDto = iOrganizationService.signUp(organizationDto, userId, createdBy, org);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @GetMapping(URIConstants.GET_ORGANIZATION)
    public ResponseEntity<OrganizationViewDto> getOrganization(@RequestParam String orgId) {
        log.info("Request receive to get organization: {}", orgId);
        OrganizationViewDto organizationDto = iOrganizationService.getOrganization(orgId, Boolean.FALSE, null);
        return ResponseEntity.status(organizationDto != null ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(organizationDto);
    }

    @GetMapping(URIConstants.GET_ORGANIZATIONS_LIST)
    public ResponseEntity<List<OrganizationViewDto>> getOrganizations(@RequestParam(required = false) String org,
                                                                      @RequestHeader("org") String userOrg) {
        if (org == null)
            org = userOrg;
        log.info("Request receive to get organizations, created by: {}", org);
        List<OrganizationViewDto> organizationDtos = iOrganizationService.getOrganizations(org);
        return ResponseEntity.status(organizationDtos != null ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(organizationDtos);
    }

    @PutMapping(URIConstants.UPDATE_ORGANIZATION)
    public ResponseEntity<ResponseDto> updateOrganizations(@RequestBody OrganizationDto organizationDto,
                                                           @RequestHeader("userId") String userId) {
        log.info("Request receive to update organization: {}", organizationDto.getOrgId());
        ResponseDto responseDto = iOrganizationService.updateOrganization(organizationDto, userId);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @PatchMapping(URIConstants.ORG_STATUS_UPDATE)
    public ResponseEntity<ResponseDto> OrgStatusUpdate(@RequestParam String orgId, @RequestParam Boolean status) {
        log.info("Request receive to disable organization: {}", orgId);
        ResponseDto responseDto = iOrganizationService.orgStatusUpdate(orgId, status);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @GetMapping(URIConstants.UN_ASSIGN_SOC)
    public ResponseEntity<ResponseDto> unAssignSoc(@RequestParam String socUserId, @RequestParam String orgId,
                                                   @RequestHeader("userId") String userId) {
        log.info("Request receive to un-assign soc for organization: {}", orgId);
        ResponseDto responseDto = iOrganizationService.unAssignSoc(socUserId, orgId, userId);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }
}
