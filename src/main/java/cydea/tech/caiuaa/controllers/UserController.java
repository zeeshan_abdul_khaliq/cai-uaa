package cydea.tech.caiuaa.controllers;

import cydea.tech.caiuaa.dtos.*;
import cydea.tech.caiuaa.services.IUserService;
import cydea.tech.caiuaa.utils.URIConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Muhammad Mujtaba, Muhammad Nabeel Rashid
 */
@Slf4j
@Validated
@RestController
@RequestMapping(URIConstants.UAA)
public class UserController {

    private IUserService iUserService;

    public UserController(IUserService iUserService) {
        this.iUserService = iUserService;
    }

    @PostMapping(URIConstants.GET_TOKEN)
    public ResponseEntity<LoginResponseDto> getToken(@RequestBody LoginRequestDto loginRequestDto) {
        log.info("Request received to generate token for user: {}", loginRequestDto.getUsername());
        LoginResponseDto loginResponseDto = iUserService.getToken(loginRequestDto);
        return ResponseEntity.status(loginResponseDto.getResponseCode()).body(loginResponseDto);
    }

    @PostMapping(URIConstants.ADD_USER)
    public ResponseEntity<ResponseDto> addUser(@Valid @RequestBody UserCreateRequestListDto createRequestListDto,
                                               @RequestHeader("username") String createdBy, @RequestHeader("org") String org) {
        log.info("Request receive to add user for org: {}", org);
        ResponseDto responseDto = iUserService.signUp(createRequestListDto, createdBy, org, false);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @PutMapping(URIConstants.UPDATE_USER)
    public ResponseEntity<ResponseDto> updateUser(@Valid @RequestBody UserUpdateRequestDto userUpdateRequestDto,
                                                  @RequestHeader("username") String username, @RequestHeader("org") String org) {
        log.info("Request receive to update user: {}", userUpdateRequestDto.getUserId());
        ResponseDto responseDto = iUserService.updateUser(userUpdateRequestDto, username, org);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @PutMapping(URIConstants.UPDATE_PROFILE)
    public ResponseEntity<ResponseDto> updateProfile(@RequestParam(required = false) String link, @Valid @RequestBody UserUpdateRequestDto userUpdateRequestDto) {
        log.info("Request receive to update profile: {}", userUpdateRequestDto.getUserId());
        ResponseDto responseDto = iUserService.updateProfile(link, userUpdateRequestDto);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @GetMapping(URIConstants.GET_USER)
    public ResponseEntity<UserDto> getUser(@RequestParam String username) {
        log.info("Request receive to get user: {}", username);
        UserDto userDto = iUserService.getUserDetail(username);
        return ResponseEntity.status(userDto != null ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(userDto);
    }

    @GetMapping(URIConstants.GET_SOC_USER)
    public ResponseEntity<UserDto> getSocUser(@RequestParam String userId) {
        log.info("Request receive to get soc user: {}", userId);
        UserDto userDto = iUserService.getUserSocDetail(userId);
        return ResponseEntity.status(userDto != null ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(userDto);
    }

    @GetMapping(URIConstants.GET_USERS_LIST)
    public ResponseEntity<UserListingDto> getUsers(@RequestParam(required = false) String org,
                                                   @RequestHeader("userId") String userId, @RequestHeader("org") String userOrg) {
        if (org == null)
            org = userOrg;
        log.info("Request receive to get users of org: {}", org);
        UserListingDto userListingDto = iUserService.getUsers(userId, org);
        return ResponseEntity.status(userListingDto != null ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(userListingDto);
    }

    @DeleteMapping(URIConstants.DELETE_USER)
    public ResponseEntity<ResponseDto> deleteUsers(@RequestParam String userId, @RequestHeader("username") String username) {
        log.info("Request received to delete user: {}", userId);
        ResponseDto responseDto = iUserService.deleteUser(userId, username);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @GetMapping(URIConstants.RESEND_LINK)
    public ResponseEntity<ResponseDto> getMagicLink(@RequestParam String magicLink) {
        log.info("Request receive to resend link");
        ResponseDto responseDto = iUserService.resendMagicLink(magicLink);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @GetMapping(URIConstants.LINK_STATUS)
    public ResponseEntity<ResponseDto> getMagicLinkStatus(@RequestParam String magicLink) {
        log.info("Request receive to check link status");
        ResponseDto responseDto = iUserService.magicLinkStatus(magicLink);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @GetMapping(URIConstants.RE_INVITE_USER)
    public ResponseEntity<ResponseDto> reInviteUser(@RequestParam String userId) {
        log.info("Request receive to re-invite user");
        ResponseDto responseDto = iUserService.reInviteUser(userId);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @PutMapping(URIConstants.CHANGE_PASSWORD)
    public ResponseEntity<ResponseDto> updatePassword(@RequestHeader("userId") String userId,
                                                      @RequestBody @Valid UserPasswordRequestDto userPasswordRequestDto) {
        log.info("Request receive to check link status");
        ResponseDto responseDto = iUserService.changePassword(userId, userPasswordRequestDto);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @PutMapping(URIConstants.ENABLE_DISABLE_USER)
    public ResponseEntity<ResponseDto> enableDisableUser(@RequestParam("user-uuid") String userId, @RequestParam("status") Boolean status) {
        ResponseDto responseDto = iUserService.enableDisablePo(userId, status);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @PostMapping(URIConstants.FORGOT_PASSWORD)
    public ResponseEntity<ResponseDto> forgotPassword(@RequestBody ForgotPasswordDto forgotPasswordDto) {
        ResponseDto responseDto = iUserService.forgotPasswordRequest(forgotPasswordDto);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @PostMapping(URIConstants.UPDATE_PASSWORD)
    public ResponseEntity<ResponseDto> updatePassword(@RequestBody ForgotPasswordLinkDto passwordLinkDto) {
        ResponseDto responseDto = iUserService.updatePassword(passwordLinkDto);
        return ResponseEntity.status(responseDto.getStatusCode()).body(responseDto);
    }

    @GetMapping(URIConstants.GET_CLIENT_ADMIN_EMAIL)
    public ResponseEntity<String> getClientAdminEmail(@RequestParam String org) {
        String emailId = iUserService.getClientAdminEmail(org);
        return ResponseEntity.status(HttpStatus.OK).body(emailId);
    }

    @GetMapping(URIConstants.CREATED_BY)
    public ResponseEntity<List<UserInfo>> getUsersCreatedBy(@RequestParam String emailId) {
        List<UserInfo> userInfos = iUserService.getUsersCreatedBy(emailId);
        return ResponseEntity.status(!userInfos.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(userInfos);
    }
}
