package cydea.tech.caiuaa.controllers;

import cydea.tech.caiuaa.dtos.UserDto;
import cydea.tech.caiuaa.services.ILookupService;
import cydea.tech.caiuaa.utils.URIConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Muhammad Mujtaba
 */
@Slf4j
@RestController
@RequestMapping(URIConstants.UAA)
public class LookupController {

    private ILookupService iLookupService;

    public LookupController(ILookupService iLookupService) {
        this.iLookupService = iLookupService;
    }

    @GetMapping(URIConstants.GET_SOC_USERS)
    public ResponseEntity<List<UserDto>> getSocUsers(@RequestHeader("userId") String userId, @RequestHeader("org") String org) {
        log.info("Request receive to get soc users");
        List<UserDto> userDtos = iLookupService.getSocUsers(userId, org);
        return ResponseEntity.status(!userDtos.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(userDtos);
    }

    @GetMapping(URIConstants.SUBSCRIPTION_TYPES)
    public ResponseEntity<List<String>> getSubscriptionTypes() {
        log.info("Request receive to get subscription types");
        List<String> subscriptionTypes = iLookupService.getSubscriptionTypes();
        return ResponseEntity.status(HttpStatus.OK).body(subscriptionTypes);
    }

    @GetMapping(URIConstants.COMPANY_ROLES)
    public ResponseEntity<List<String>> getCompanyRoles(@RequestHeader("userId") String userId) {
        log.info("Request receive to get company roles");
        List<String> companyRoles = iLookupService.getCompanyRoles(userId);
        return ResponseEntity.status(!companyRoles.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(companyRoles);
    }

    @GetMapping(URIConstants.ADDITIONAL_ROLES)
    public ResponseEntity<List<String>> getAdditionalRoles(@RequestHeader("userId") String userId) {
        log.info("Request receive to get additional roles");
        List<String> additionalRoles = iLookupService.getAdditionalRoles(userId);
        return ResponseEntity.status(!additionalRoles.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT)
                .body(additionalRoles);
    }

    @GetMapping(URIConstants.SOC_ADDITIONAL_ROLE)
    public ResponseEntity<List<String>> getSocAdditionalRole(@RequestHeader("userId") String userId) {
        log.info("Request receive to get soc additional role");
        List<String> additionalRoles = iLookupService.getSocAdditionalRole(userId);
        return ResponseEntity.status(!additionalRoles.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT)
                .body(additionalRoles);
    }

    @GetMapping(URIConstants.SECTORS)
    public ResponseEntity<List<String>> getSectors() {
        log.info("Request receive to get sectors");
        List<String> sectors = iLookupService.getSectors();
        return ResponseEntity.status(HttpStatus.OK).body(sectors);
    }

    @GetMapping(URIConstants.ACCESS_ROLES)
    public ResponseEntity<List<String>> getAccessRoles(@RequestHeader("userId") String userId,
                                                       @RequestParam(defaultValue = "false") Boolean soc) {
        log.info("Request receive to get access roles");
        List<String> accessRoles = iLookupService.getAccessRoles(userId, soc);
        return ResponseEntity.status(!accessRoles.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT).body(accessRoles);
    }
}
