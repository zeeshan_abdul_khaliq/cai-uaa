package cydea.tech.caiuaa.enumerations;

/**
 * @author Muhammad Mujtaba
 */
public enum UserStatusEnum {
    ACTIVE,
    DISABLED,
    INVITED
}
