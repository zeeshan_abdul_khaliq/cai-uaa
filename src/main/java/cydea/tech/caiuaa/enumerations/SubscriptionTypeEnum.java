package cydea.tech.caiuaa.enumerations;

/**
 * @author Muhammad Mujtaba
 */
public enum SubscriptionTypeEnum {
    Trial,
    Full
}
