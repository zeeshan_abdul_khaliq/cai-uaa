package cydea.tech.caiuaa.enumerations;

/**
 * @author Muhammad Mujtaba
 */
public enum SectorEnum {
    Hospitality,
    Technology,
    Retail,
    Construction,
    Engineering,
    Manufacturing,
    Justice,
    Gas,
    Education,
    Critical_Infrastructure,
    Telecommunication,
    Logistics,
    IT,
    Government,
    Medical,
    Oil,
    Finance_Insurance,
    Defense,
    Energy,
    Media,
    Aerospace,
    Pharmaceuticals,
    Bitcoin_Exchanges,
    Aviation,
    Petrochemicals,
    Automotive,
    Chemical,
    Transportation,
    Food_And_Agriculture,
    Gaming,
    NGOs,
    Others
}
