package cydea.tech.caiuaa.enumerations;

/**
 * @author Muhammad Mujtaba
 */
public enum CompanyRoleEnum {
    Admin,
    Distributor,
    Partner,
    Client
}
